 $('.datatables-demo').dataTable();
		$.ajax({
			url:'estados.php',
			success: function(res){ $("#est1").html(res); $("#est2").html(res);},
			error: function(err){ $("#est").html(err);}
		});

			$('#est2').change(function() {
				datos = {'estado': $('#est2').val()};
				$.ajax({
					data: datos,
					url:'ciudad.php',
					method: "POST",
					success: function(res){ $("#ciu").html(res);},
					error: function(err){ $("#ciu").html(err);}
				});
			});

			$('#comprobarIdEstado').on('click', function() {
				datos = {'nombre': $('#nombreEstado').val().slice(0,3)};
				$.ajax({
					data: datos,
					url:'comprobarIdEstado.php',
					method: "POST",
					success: function(res){ 
						$("#codigoEstado").val(res);
						$("#guardar_estado").css({display: 'initial'});
					},
					error: function(err){ $("#codigoEstado").val(err);}
				});
			});

			$('#comprobarIdCiudad').on('click', function() {
				datos = {'nombre': $('#nombreCiudad').val().slice(0,3)};
				$.ajax({
					data: datos,
					url:'comprobarIdCiudad.php',
					method: "POST",
					success: function(res){ 
						$("#codigoCiudad").val(res);
						$("#guardar_ciudad").css({display: 'initial'});
					},
					error: function(err){ $("#codigoEstado").val(err);}
				});
			});

			$('#comprobarIdMunicipio').on('click', function() {
				datos = {'nombre': $('#nombreMunicipio').val().slice(0,3)};
				$.ajax({
					data: datos,
					url:'comprobarIdMunicipio.php',
					method: "POST",
					success: function(res){ 
						$("#codigoMunicipio").val(res);
						$("#guardar_municipio").css({display: 'initial'});
					},
					error: function(err){ $("#codigoMunicipio").val(err);}
				});
			});
// ****************************************************************
			
			$('#nombreEstado').keyup(function(){
				if (document.getElementById('nombreEstado').value.length < 3){
						$("#guardar_estado").css({display: 'none'});
						$("#codigoEstado").val('');
						$("#comprobarIdEstado").css({display: 'none'});
				}else{
						$("#comprobarIdEstado").css({display: 'initial'});
				}
			});

			$('#nombreCiudad').keyup(function(){
				if (document.getElementById('nombreCiudad').value.length < 3){
						$("#guardar_ciudad").css({display: 'none'});
						$("#codigoCiudad").val('');
						$("#comprobarIdCiudad").css({display: 'none'});
				}else{
						$("#comprobarIdCiudad").css({display: 'initial'});
				}
			});

			$('#nombreMunicipio').keyup(function(){
				if (document.getElementById('nombreMunicipio').value.length < 3 || document.getElementById('ciu').value == ''){
						$("#guardar_municipio").css({display: 'none'});
						$("#codigoMunicipio").val('');
						$("#comprobarIdMunicipio").css({display: 'none'});
				}else{
						$("#comprobarIdMunicipio").css({display: 'initial'});
				}
			});
			$('#ciu').click(function(){
				if (document.getElementById('nombreMunicipio').value.length < 3 || document.getElementById('ciu').value == ''){
						$("#guardar_municipio").css({display: 'none'});
						$("#codigoMunicipio").val('');
						$("#comprobarIdMunicipio").css({display: 'none'});
				}else{
						$("#comprobarIdMunicipio").css({display: 'initial'});
				}
			});

// ****************************************************************

			$("#guardar_estado").click(function(){
				datos = {'codigo': $('#codigoEstado').val(),'nombre' : $('#nombreEstado').val()};
				$.ajax({
					data: datos,
					url:'saveEstado.php',
					method: "POST",
					success: function(res){
						Swal.fire({
						  position: 'center',
						  type: 'success',
						  title: 'Guardado',
						  showConfirmButton: false,
						  timer: 1500
						});
						$("#codigoEstado").val('');
						$("#nombreEstado").val('');
						$("#guardar_estado").css({display: 'none'});
						$("#codigoEstado").val('');
						$("#comprobarIdEstado").css({display: 'none'});
					},
					error: function(err){ $("#codigoEstado").val(err);}
				});
			});

			$("#guardar_ciudad").click(function(){
				datos = {'codigo': $('#codigoCiudad').val(),'nombre' : $('#nombreCiudad').val(),'estado' : $('#est1').val()};
				$.ajax({
					data: datos,
					url:'saveCiudad.php',
					method: "POST",
					success: function(res){
						Swal.fire({
						  position: 'center',
						  type: 'success',
						  title: 'Guardado',
						  showConfirmButton: false,
						  timer: 1500
						});
						$("#codigoCiudad").val('');
						$("#nombreCiudad").val('');
						$("#guardar_ciudad").css({display: 'none'});
						$("#comprobarIdCiudad").css({display: 'none'});
					},
					error: function(err){ $("#codigoEstado").val(err);}
				});
			});

			$("#guardar_municipio").click(function(){
				datos = {'codigo': $('#codigoMunicipio').val(),'nombre' : $('#nombreMunicipio').val(),'ciudad' : $('#ciu').val()};
				$.ajax({
					data: datos,
					url:'saveMunicipio.php',
					method: "POST",
					success: function(res){
						Swal.fire({
						  position: 'center',
						  type: 'success',
						  title: 'Guardado',
						  showConfirmButton: false,
						  timer: 1500
						});
						$("#codigoMunicipio").val('');
						$("#nombreMunicipio").val('');
						$("#guardar_municipio").css({display: 'none'});
						$("#comprobarIdMunicipio").css({display: 'none'});
					},
					error: function(err){ $("#codigoEstado").val(err);}
				});
			});

// **********************************************************************************************

  function eliminarEstado($codigo){
    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar este estado!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, eliminar!'
}).then((result) => {
  if (result.value) {
  	datos = {'codigo': $codigo};
	$.ajax({
		data: datos,
		url:'eliminarEstado.php',
		method: "POST",
		success: function(res){ Swal.fire({ type: 'success',  title: 'Estado eliminado',  showConfirmButton: false,  timer: 1500	});},
		error: function(err){ Swal.fire({	type: 'error', title: 'Oops...', text: 'Algo no salio bien!',});}
	});
  }
});
}
  function eliminarCiudad($codigo){
    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar esta ciudad!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, eliminar!'
}).then((result) => {
  if (result.value) {
  	datos = {'codigo': $codigo};
	$.ajax({
		data: datos,
		url:'eliminarCiudad.php',
		method: "POST",
		success: function(res){ Swal.fire({ type: 'success',  title: 'Ciudad eliminada',  showConfirmButton: false,  timer: 1500	});},
		error: function(err){ Swal.fire({	type: 'error', title: 'Oops...', text: 'Algo no salio bien!',});}
	});
  }
});
}

  function eliminarMunicipio($codigo){
    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar este municipio!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar', 
    confirmButtonText: 'Si, eliminar!'
}).then((result) => {
  if (result.value) {
  	datos = {'codigo': $codigo};

	$.ajax({
		data: datos,
		url:'eliminarMunicipio.php',
		method: "POST",
		success: function(res){

		if (res == 0) {
			Swal.fire({ type: 'success',  title: 'Municipio eliminado',  showConfirmButton: false,  timer: 1500	});
		}else{
			Swal.fire({ type: 'error',  title: 'Disculpa, hay un municipio destinado a esta direccion', timer: 1500	});
		}

		},
		error: function(err){ Swal.fire({ type: 'error', title: 'Oops...', text: 'Algo no salio bien!',});}
	});
  }
});
}
