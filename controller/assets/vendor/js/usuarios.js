
  $('.datatables-demo').dataTable();

function eliminarTazador($codigo){

    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar este usuario!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, eliminar!'

}).then((result) => {
  if (result.value) {

      dato = { "codigo" : $codigo};
        $.ajax({
        data: dato,
        url:'eliminarTazador.php',
        method: "POST",
        success: function(res){ $("#titulo").append(res);

        $.ajax({
        url:'tazadores.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });

      },
        error: function(err){ $("#titulo").append(err);}
      });

    Swal.fire('Eliminar!','Usuario eliminado.','success');
  }
});

}//eliminarTazador

  function editarTazador($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        method: 'POST',
        url:'editarTazador.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }//editarTazador

  function recordTazador($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        method: 'POST',
        url:'recordTazador.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }//editar Tazador




// ****************************************************


  $('.datatables-demo').dataTable();

function eliminarAdministrador($codigo){

    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar este usuario!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, eliminar!'

}).then((result) => {
  if (result.value) {

      dato = { "codigo" : $codigo};
        $.ajax({
        data: dato,
        url:'eliminarAdministrador.php',
        method: "POST",
        success: function(res){ $("#titulo").append(res);

        $.ajax({
        url:'administradores.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });

      },
        error: function(err){ $("#titulo").append(err);}
      });

    Swal.fire('Eliminar!','Usuario eliminado.','success')
  }
});

}//eliminarTazador

  function editarAdministrador($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        method: 'POST',
        url:'editarAdministrador.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }//editarTazador

  function recordTazador($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        method: 'POST',
        url:'recordTazador.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }//editar Tazador