
  $('.datatables-demo').dataTable();

  function eliminarTazacion($codigo){

    Swal.fire({
    title: 'Esta seguro?',
    text: "Quieres eliminar esta Tasación!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, eliminar!'

}).then((result) => {
  if (result.value) {

      dato = { "codigo" : $codigo};
        $.ajax({
        data: dato,
        url:'eliminarTazacion.php',
        method: "POST",
        success: function(res){ $("#titulo").append(res);
        Swal.fire('Eliminar!','Tazacion eliminada.','success');
        $.ajax({
        url:'tazaciones.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });

      },
        error: function(err){ $("#titulo").append(err);}
      });

  }
});

}//eliminarTazacion

  function editarTazacion($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        url:'editarTazacion.php',
        method: "POST",
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }

    $('#telefono').on('input', function () { 
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    $('#guardar').on('click',function(){

      if ($('#nombre').val() == '' ||  $('#correo').val() == ''  || $('#telefono').val() == ''  || $('#clave').val() == '') {

    Swal.fire({
      title: 'Por favor complete los campos',
      type: 'info'
    });

      }else{

    dato = { 
      "nombre" : $('#nombre').val(),
      "correo" : $('#correo').val(),
      "telefono" : $('#telefono').val(),
      "clave" : $('#clave').val()
    };
      $.ajax({
      data: dato,
      url:'guardarEdicionTazador.php',
      method: "POST",
      success: function(res){ $("#init_content").html(res);

      $.ajax({
        url:'tazadores.php',
        success: function(res){ $("#init_content").html(res);},
        error: function(err){   $("#init_content").html(err);}
      });
  },
      error: function(err){   $("#init_content").html(err);}
    });
  }
  });