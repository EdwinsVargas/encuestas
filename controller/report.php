<?php session_start();
require '../model/tazaciones.php';
require_once dirname(__FILE__).'/../vendor/autoload.php';

	$tazaciones =  new tazaciones();
	$reporte = $tazaciones->getReport($_SESSION['brazo']);
	$galeria = $tazaciones->getGaleria($_SESSION['brazo']);

		use Spipu\Html2Pdf\Html2Pdf;
		use Spipu\Html2Pdf\Exception\Html2PdfException;
		use Spipu\Html2Pdf\Exception\ExceptionFormatter;

	try{
   		ob_start();

    	include '../view/report.php';

		$content = ob_get_clean();
		$document = new Html2Pdf('P', 'A4', 'fr', true, 'UTF-8', 3);
		$document->writeHTML($content);
		$document->Output(dirname(__FILE__).'/assets/reportes/Tasacion '.$_SESSION['brazo'].'.pdf','F');
		header('location:../');
	} catch (Html2PdfException $e) {

		$document->clean();
	    $formatter = new ExceptionFormatter($e);
	    echo $formatter->getHtmlMessage();

	}
  ?>