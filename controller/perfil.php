<?php session_start();
if ($_SESSION['status']==1) {


 $_SESSION['location'] = 'perfil.php';
require '../model/usuario.php';
require '../model/tazaciones.php';

$user = new usuario();

$usuario = $user->getUser($_SESSION['correo']);

$ordenes_atendidas = count($user->getHistorialDe($_SESSION['correo']));

switch ($usuario['rol']) {
	case '1':
		$generar = 'checkmark';
		$resolver = 'checkmark';
		$eliminar = 'checkmark';
		$gestionar = 'checkmark';
		$rol='Root super usuario';
	break;
	case '2':
		$generar = 'checkmark';
		$resolver = 'close';
		$eliminar = 'checkmark';
		$gestionar = 'close';
		$rol='Administrador';
	break;
	case '3':
		$generar = 'close';
		$resolver = 'checkmark';
		$eliminar = 'close';
		$gestionar = 'close';
		$rol='Tasador';
	break;
	default:break;
}


require '../view/perfil.php';

}else{
	session_destroy();
	header('location:../');
} ?>
