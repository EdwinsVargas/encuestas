<?php session_start(); 

$_SESSION['location'] = 'historial.php';

require '../model/formulario.php';

$_FORMULARIO = new formulario($_SESSION['correo'],$_POST['codigo']);

try {
	
$_FORMULARIO->newBasicos(
	$_POST['fechaInspeccion'],
	$_POST['fechaEntregaCorta'],
	$_POST['fechaEntregaLarga'],
	$_POST['nombreSolicitante'],
	$_POST['apellidoSolicitante'],
	$_POST['direccionProyecto'],
	$_POST['numeroInmueble'],
	$_POST['sector'],
	$_POST['propietarios'],
	$_POST['documentoLegal'],
	$_POST['numeroDocumento'],
	$_POST['designacionCatastral'],
	$_POST['folio'],
	$_POST['libro'],
	$_POST['fechaExpedicion'],
	$_POST['antiguedadPropiedad'],
	$_POST['antiguedadEntorno'],
	$_POST['ocupadaPor'],
	$_POST['tipoInmueble'],
	$_POST['areaSolar'],
	$_POST['areaMejora'],
	$_POST['linderoNorte'],
	$_POST['linderoSur'],
	$_POST['linderoEste'],
	$_POST['linderoOeste'],
	$_POST['distanciaEscuelas'],
	$_POST['distanciaTransporte'],
	$_POST['distanciaComercio'],
	$_POST['distanciaCentroCiudad'],
	$_POST['nivelesConstruidosInmueble'],
	$_POST['nivelesConstruidosEstructura'],
	$_POST['nivelInmueble'],
	$_POST['ventana'],
	$_POST['puertaPrincipal'],
	$_POST['puertasInteriores'],
	$_POST['closets'],
	$_POST['wClosets'],
	$_POST['gabinetesCocina'],
	$_POST['topeCocina'],
	$_POST['paredCocina'],
	$_POST['banos'],
	$_POST['revestimientoBanos'],
	$_POST['techos'],
	$_POST['condicionesGeneralesInmueble'],
	$_POST['coordenadasGeo'],
	$_POST['observaciones']);

$_FORMULARIO->newAmbientes(
	$_POST['entradaLobby'],
	$_POST['marquesina'],
	$_POST['galeria'],
	$_POST['recibidor'],
	$_POST['sala'],
	$_POST['comedor'],
	$_POST['cocina'],
	$_POST['cocinaFriaCaliente'],
	$_POST['cocinaDesayunador'],
	$_POST['cocinaIsla'],
	$_POST['bano'],
	$_POST['salaEstar'],
	$_POST['areaLavado'],
	$_POST['cuartoServicio'],
	$_POST['balcon'],
	$_POST['terraza'],
	$_POST['patio']);

$_FORMULARIO->newAmenidades($_POST['amenidades']);

$_FORMULARIO->newBanos($_POST['banoNivel1'],$_POST['banoNivel2'],$_POST['banoNivel3']);

$_FORMULARIO->newAdicionales($_POST['adicionales']);

$_FORMULARIO->newWalkingCloset($_POST['wClosetNivel1'],$_POST['wClosetNivel2']);

$_FORMULARIO->newPisos($_POST['pisoNivel1'],$_POST['pisoNivel2']);

$_FORMULARIO->newClosets($_POST['closetNivel1'],$_POST['closetNivel2'],$_POST['closetNivel3']);

$_FORMULARIO->newParqueos($_POST['parqueosAireLibre'],$_POST['parqueosTechado']);

$_FORMULARIO->newDormitorios($_POST['dormitoriosGrandes'],$_POST['dormitoriosMedianos'],$_POST['dormitoriosPequenos']);

$_FORMULARIO->newFormulario();

header('location:../');

} catch (SQException $ex) {
	$_FORMULARIO->getConexion()->rollback();
}

 ?>
