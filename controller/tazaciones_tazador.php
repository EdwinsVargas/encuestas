<?php session_start(); $_SESSION['location'] = 'tazaciones_tazador.php';

if ($_SESSION['status']==1) {

	require '../model/tazaciones.php';
	require '../model/root.php';
	$_TAZACIONES = new tazaciones();
	$tazaciones = $_TAZACIONES->getTazacionesDisponibles();
	$estados = $_TAZACIONES->getEstados();
	require '../view/tazaciones_tazador.php';


}else{
	session_destroy();
	header('location:../');
}
 ?>
