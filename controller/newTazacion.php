<?php session_start();
if ($_SESSION['status']==1) {

 require '../model/tazaciones.php';
 
 $_TAZACION = new tazaciones();
 
 $codigo = $_TAZACION->verificacionDuplicados('tazaciones','idtazaciones','TAZ');
 
 $estados = $_TAZACION->getEstados();
 
 $hoy = getdate();

 require '../view/newTazacion.php';
 
}else{
	session_destroy();
	header('location:../');
} 
?>