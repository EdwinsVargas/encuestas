<?php session_start();

if(isset($_SESSION['rol'])){

	switch ($_SESSION['rol']) {
		case '1':	require '../view/dashboard_root.php';		break;
		case '2':	require '../view/dashboard_admin.php';		break;
		case '3':	require '../view/dashboard_tazador.php';	break;
		default:	require '../view/404.php';					break;
	}
}else{
	header('location:../');
}
 ?>
