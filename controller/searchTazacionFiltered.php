<?php session_start();
require '../model/tazaciones.php';

$tazaciones = new tazaciones();

$tazaciones = $tazaciones->getTazacionesByCiudad($_POST['ciudad']);

 ?>
 <?php if (isset($tazaciones)){ ?>
	<?php foreach ($tazaciones as $tazacion){  $IDTAZACION = $tazacion['idtazaciones']; ?>
	 			<div id="cartas" class="col-sm-12 col-md-6 col-lg-4">
                  <div class="card mb-3">
                    <div class="card-header">
                        <?php if ($tazacion['solved'] == '1'){ ?>
                        <span  style="padding: 5px 10px; width: 100px;" class="badge badge-success">Resuelto <i class="fas fa-check"></i></span>
                        <?php }else if ($tazacion['solved'] == '0'){ ?>
                        <span  style="padding: 5px 10px; width: 100px;" class="badge  badge-default">Sin resolver <i class="fas fa-pencil-alt"></i></span>
                        <?php } ?>
                    </div>
                    <div class="card-body">
                      <h4 class="card-title">Cliente <?=$tazacion['cliente']?></h4>
                      <p class="card-text"><i class="fas fa-phone"></i>         <strong>Telefono: </strong><?=$tazacion['telefonoCliente']?></p>
                      <p class="card-text"><i class="fas fa-map-marker-alt"></i> <strong>Direccion: </strong><?=$tazacion['direccion'].'. '.$tazacion['municipio'].', '.$tazacion['ciudad'].' '.$tazacion['estado']?></p>
                      <p class="card-text"><i class="far fa-clock"></i>         <strong>Fecha de creacion: </strong><?=$tazacion['fechaGeneracion']?></p>
                      <a href="javascript:void(0)" id="<?=$IDTAZACION?>" onclick="clicked('<?=$IDTAZACION?>')" data-datos='<?=$IDTAZACION?>' data-url='formulario.php' data-cont='init_content' class="btn btn-primary"><i class="fas fa-edit"></i> Atender orden</a>
                    </div>
                  </div>
                </div>
	<?php }?>

 <?php }else{ echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasaciones actualmente<h6></div>"; } ?>