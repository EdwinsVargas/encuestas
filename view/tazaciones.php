
<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row media align-items-center justify-content-center justify-content-sm-start">
          <div class="col-8 col-sm-8 col-md-9 col-lg-10 d-flex"><h4  class="font-weight-bold mb-0 media-body ml-3"><span class=""><i class="fas fa-tasks"> </i> Tasaciones</span></h4></div>

    <?php if ($_SESSION['rol'] == 2 or $_SESSION['rol'] == 1): ?>
          <div class="col-4 col-sm-4 col-md-3 col-lg-2  d-flex"><button class="btn btn-primary btn-ms-block" id="newTazacion" onclick="clicked('newTazacion')" data-url='newTazacion.php' data-cont='init_content'><span>Nueva Tasación</span> <i class="fas fa-plus"></i></button></div>
    <?php endif ?>

    </div>
</div>

<div class="container-fluid flex-grow-1 container-p-y">

      <div class="card">
              <h6 class="card-header" id="titulo">
                Tasaciones Registradas
              </h6>
              <div class="card-datatable table-responsive">

              <?php if (isset($tazaciones)) { ?>  

                <table class="datatables-demo table table-striped ">
                  <thead>
                    <tr>
                  
                  <?php if ($_SESSION['rol'] < 3): ?>
                      <th></th>
                  <?php endif ?>
                  
                      <th>Código</th>
                      <th>Cliente</th>
                      <th>Contacto</th>
                      <th>Dirección</th>
                      <th>Fecha</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody>

        <?php foreach ($tazaciones as $tazacion) { $IDTAZACION = $tazacion['idtazaciones']; ?>
  
                      <tr class="odd gradeX" style="cursor: pointer;"

                      <?php if ($_SESSION['rol'] >= 3 && $tazacion['solved'] == 0): ?> 

                        id="<?=$IDTAZACION?>" onclick="clicked('<?=$IDTAZACION?>')" data-datos='<?=$IDTAZACION?>' data-url='formulario.php' data-cont='init_content'
                      
                      <?php endif ?>
                      >

                      <?php if ($_SESSION['rol']<3): ?>
                          <td style="vertical-align: baseline;">
                            <button onclick="eliminarTazacion('<?=$tazacion['idtazaciones']?>')" class="btn btn-default"><i class="fas fa-trash-alt"></i></button>
                            <button onclick="editarTazacion('<?=$tazacion['idtazaciones']?>');" id='editar' data-url='editarTazacion.php' data-cont='init_content' class="btn btn-default"><i class="fas fa-edit"></i></button>
                          </td>
                      <?php endif ?>
                          <td style="vertical-align: baseline;" class="center"><?=$tazacion['idtazaciones']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$tazacion['cliente']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$tazacion['telefonoCliente']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$tazacion['direccion'].'. '.$tazacion['municipio'].', '.$tazacion['ciudad'].' '.$tazacion['estado']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$tazacion['fechaGeneracion']?></td>
                          <td style="vertical-align: baseline;" class="center">
                            <?php if ($tazacion['solved'] == '1'){ ?>
                            <span  style="padding: 5px 10px; width: 100px;" class="badge badge-pill badge-success">Resuelto <i class="fas fa-check"></i></span>
                            <?php }else if ($tazacion['solved'] == '0'){ ?>
                            <span  style="padding: 5px 10px; width: 100px;" class="badge badge-pill badge-default">Sin resolver <i class="fas fa-pencil-alt"></i></span>
                            <?php } ?>
                      </tr>
                <?php  } ?>
                  </tbody>
           
                </table>
                   <?php }else{echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasaciones registradas actualmente<h6></div>";} ?>
              </div>
            </div>
</div>

<script src="assets/vendor/js/tazaciones.js" ></script>