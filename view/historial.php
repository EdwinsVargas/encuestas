

<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row media align-items-center justify-content-center justify-content-sm-start">
          <div class="col-8 col-sm-8 col-md-9 col-lg-10 d-flex"><h4  class="font-weight-bold mb-0 media-body ml-3"><span class=""><i class="fas fa-history"> </i> Tasaciones realizadas</span></h4></div>
    </div>
</div>

<div class="container-fluid flex-grow-1 container-p-y">

      <section id="cards_cont" class="row" style="display: flex;">
        <?php if (isset($historiales)) { ?>  
                <?php foreach ($historiales as $historial) { $IDTAZACION = $historial['idtazaciones']; ?>
                        <div id="cartas" class="col-sm-12 col-md-6 col-lg-4">
                          <div class="card mb-3">
                            <div class="card-header">
                                <?php if ($historial['solved'] == '1'){ ?>
                                <span  style="padding: 5px 10px; width: 100px;" class="badge badge-success">Resuelto <i class="fas fa-check"></i></span>
                                <?php }else if ($historial['solved'] == '0'){ ?>
                                <span  style="padding: 5px 10px; width: 100px;" class="badge  badge-default">Sin resolver <i class="fas fa-pencil-alt"></i></span>
                                <?php } ?>
                            </div>
                            <div class="card-body">
                              <h4 class="card-title">Cliente <?=$historial['cliente']?></h4>
                              <p class="card-text"><i class="fas fa-phone"></i>          <strong>Teléfono: </strong><?=$historial['telefonoCliente']?></p>
                              <p class="card-text"><i class="fas fa-map-marker-alt"></i> <strong>Dirección: </strong><?=$historial['direccion'].'. '.$historial['municipio'].', '.$historial['ciudad'].' '.$historial['estado']?></p>
                              <p class="card-text"><i class="far fa-clock"></i>          <strong>Fecha de creación: </strong><?=$historial['fechaGeneracion']?></p>
                              <?php if (!$usuario->isContainGallery($historial['idtazaciones'])){ ?>
                                  <button title="Incluir galeria"  onclick="incluirGaleria(<?="'".$historial['idtazaciones']."'"?>)" class="btn btn-warning"> <i class=" fas fa-camera"></i> Incluir galeria</button>
                              <?php }else{ ?>
                                  <a title="Descargar reporte" download="Tasacion <?=$historial['idtazaciones'].'.pdf'?>" href="assets/reportes/Tasacion <?=$historial['idtazaciones'].'.pdf'?>" class="btn btn-info"><i class="fas fa-file-pdf"></i> Guardar Reporte </a>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                <?php } ?>
        <?php }else{echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasaciones registradas actualmente<h6></div>";} ?>
      </section>
</div>

<script>
  $('.datatables-demo').dataTable();

  function eliminarTazacion($codigo){
      dato = { "codigo" : $codigo};
        $.ajax({
        data: dato,
        url:'eliminarTazacion.php',
        method: "POST",
        success: function(res){ $("#titulo").append(res);},
        error: function(err){ $("#titulo").append(err);}
      });
  }

  function editarTazacion($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        url:'editarTazacion.php',
        method: "POST",
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }

  function incluirGaleria($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        url:'galeria.php',
        method: "POST",
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }
</script>