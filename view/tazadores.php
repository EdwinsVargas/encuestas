<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row media align-items-center justify-content-center justify-content-sm-start">
          <div class="col-8 col-sm-8 col-md-9 col-lg-10 d-flex"><h4  class="font-weight-bold mb-0 media-body ml-3"><span class=""><i class="fas fa-user"> </i> Tasadores</span></h4></div>
          <div class="col-4 col-sm-4 col-md-3 col-lg-2  d-flex"><button class="btn btn-primary btn-ms-block" id="newTazador" onclick="clicked('newTazador')" data-url='newTazador.php' data-cont='init_content'><span>Nuevo tasador</span> <i class="fas fa-plus"></i></button></div>
    </div>
</div>

<div class="container-fluid flex-grow-1 container-p-y">

      <div class="card">
              <h6 class="card-header" id="titulo">
                Tazadores registrados
              </h6>
              <div class="card-datatable table-responsive">
                
                <table class="datatables-demo table table-striped ">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Correo</th>
                      <th>Nombre</th>
                      <th>Teléfono</th>
                      <th>Clave</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody>

                   <?php if (isset($usuarios)) {
                   foreach ($usuarios as $usuario) { ?>

                      <tr class="odd gradeX">
                          <td style="vertical-align: baseline;">
                            <button title="Eliminar" onclick="eliminarTazador('<?=$usuario['correo']?>')" class="btn btn-default"> <i class="fas fa-trash-alt"></i></button>
                            <button title="Modificar" onclick="editarTazador('<?=$usuario['correo']?>');"  class="btn btn-default"> <i class="fas fa-edit"></i></button>
                            <button title="Record" onclick="recordTazador('<?=$usuario['correo']?>');"  class="btn btn-default"> <i class="fas fa-list"></i></button>
                          </td>
                          <td style="vertical-align: baseline;" class="center"><?=$usuario['correo']?>   </td>
                          <td style="vertical-align: baseline;" class="center"><?=$usuario['nombre']?>   </td>
                          <td style="vertical-align: baseline;" class="center"><?=$usuario['telefono']?> </td>
                          <td style="vertical-align: baseline;" class="center"><?=$usuario['clave']?>    </td>
                          <td style="vertical-align: baseline;" class="center text-center">
                              <?php if ($usuario['status'] == '1'){ ?>
                              <span style="padding: 5px 10px; width: 90px;" class="badge badge-pill badge-success">Activo <i class="fas fa-check"></i></span>
                              <?php }else if ($usuario['status'] == '0'){ ?>
                              <span style="padding: 5px 10px; width: 90px;" class="badge badge-pill badge-danger">Inactivo <i class="fas fa-user-slash"></i></span>
                              <?php } ?>
                          </td>
                      </tr>

                  <?php  } }else{echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasadores registrados actualmente<h6></div>";} ?>

                  </tbody>
                </table>
              </div>
            </div>
</div>
<script src="assets/vendor/js/usuarios.js"></script>