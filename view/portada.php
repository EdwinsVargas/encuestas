
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-2">
    <div class="layout-inner">

      <!-- Layout container -->
      <div class="layout-container">

        <!-- Layout content -->
        <div class="layout-content">

          <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">

            <h4 class="font-weight-bold py-3 mb-4">
              Bienvenido,  <?=$_SESSION['nombre']?>
              <div class="text-muted text-tiny mt-1">
                <small class="font-weight-normal">Hoy es <?php echo date('d - M - Y'); ?></small>
              </div>
            </h4>

            <!-- Counters -->
            <div class="row">
              <div class="col-sm-12 col-xl-4">

                <div class="card mb-4">
                  <div class="card-body">
                    <div class="d-flex align-items-center">
                      <div class="ion ion-md-list d-block   display-4 text-info"></div>
                      <div class="ml-3">
                        <div class="text-muted small">Ordenes registradas</div>
                        <div class="text-large">
                          <?= $ordenes_registradas ?>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-sm-12 col-xl-4">

                <div class="card mb-4">
                  <div class="card-body">
                    <div class="d-flex align-items-center">
                      <div class="ion ion-md-checkmark-circle-outline d-block  display-4 text-success"></div>
                      <div class="ml-3">
                        <div class="text-muted small">Ordenes disponibles</div>
                        <div class="text-large">
                           <?=$ordenes_disponibles ?>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-xl-4">

                <div class="card mb-4">
                  <div class="card-body">
                    <div class="d-flex align-items-center">
                      <div class="lnr lnr-users display-4 text-warning"></div>
                      <div class="ml-3">
                        <div class="text-muted small">Usuarios</div>
                        <div class="text-large">
                           <?=$usuarios ?> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- / Counters -->

            <!-- Statistics -->
            <div class="card mb-4">
              <div class="row no-gutters row-bordered">
                <div class="col-md-4 col-lg-12 col-xl-4">
                  <div class="card-body">
                    <!-- Numbers -->
                    <div class="row">
                      <div class="col-6 col-xl-5 text-muted mb-3">Total sales</div>
                      <div class="col-6 col-xl-7 mb-3">
                        <span class="text-big">10,332</span>
                        <sup class="text-success">+12%</sup>
                      </div>
                      <div class="col-6 col-xl-5 text-muted mb-3">Income amount</div>
                      <div class="col-6 col-xl-7 mb-3">
                        <span class="text-big">$1,534</span>
                        <sup class="text-danger">-5%</sup>
                      </div>
                      <div class="col-6 col-xl-5 text-muted mb-3">Total budget</div>
                      <div class="col-6 col-xl-7 mb-3">
                        <span class="text-big">$10,534</span>
                        <sup class="text-success">+12%</sup>
                      </div>
                      <div class="col-6 col-xl-5 text-muted mb-3">Page views</div>
                      <div class="col-6 col-xl-7 mb-3">
                        <span class="text-big">21,332</span>
                        <sup class="text-danger">-12%</sup>
                      </div>
                      <div class="col-6 col-xl-5 text-muted mb-3">Completed tasks</div>
                      <div class="col-6 col-xl-7 mb-3">
                        <span class="text-big">12</span>
                        <sup class="text-success">+12%</sup>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- / Statistics -->
          </div>
          <!-- / Content -->


        </div>
        <!-- Layout content -->

      </div>
      <!-- / Layout container -->

    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
  </div>
  <!-- / Layout wrapper -->

  <!-- Demo -->
  <script src="assets/js/demo.js"></script>
  <script src="assets/js/dashboards_dashboard-1.js"></script>
