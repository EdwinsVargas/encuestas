
<div class="container-fluid flex-grow-1 container-p-y">
	<div class="card mb-4">
		<div class="container-fluid flex-grow-1 container-p-y card-header">
			<h4 class="media align-items-center font-weight-bold  mb-0">
			<div class="media-body ml-3 col-12"><i class="fas fa-list"></i> Editar Usuario</div>
			</h4>
		</div>
		  <form class="card-body" method="POST">
	             <div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Nombre del tasador</label> 
		                <input id="nombre" type="text" class="form-control" placeholder="Tu respuesta"  value="<?=$usuario['nombre']?>">
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Correo electronico</label> 
		                <input id="correo" readonly="readonly" type="text" class="form-control" placeholder="Tu respuesta"  value="<?=$usuario['correo']?>">
	                </div>
				</div>
				<div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Teléfono</label> 
		                <input id="telefono" type="tel" class="form-control" placeholder="Tu respuesta"  value="<?=$usuario['telefono']?>" pattern="[0-9]">
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Clave</label> 
		                <input id="clave" type="text" class="form-control" placeholder="Tu respuesta"  value="<?=$usuario['clave']?>">
	                </div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label class="switcher switcher-lg">
	                    	<input id="status" name="status" type="checkbox" <?php if ($usuario['status'] == 1):  echo 'checked';  endif ?> class="switcher-input">
		                    <span class="switcher-indicator">
			                      <span class="switcher-yes">
			                        	<span class="ion ion-md-checkmark"></span>
			                      </span>
			                      <span class="switcher-no">
			                        	<span class="ion ion-md-close"></span>
			                      </span>
			                    </span>
	                    	<span class="switcher-label">Activo</span>
	                  </label>
					</div>
				</div>
				<br>
				<button id="guardarTazador" type="button" class="btn btn-primary">Guardar cambios</button>
				<button id="volver" type="button" class="btn btn-default" value="<?=$usuario['rol']?>">Cancelar</button>
		  </form>
	</div>
</div>

<script>
		var status = $('#status').is(':checked') ? 1 : 0;

		$('#status').on('click',function() {
		    status = $('#status').is(':checked') ? 1 : 0;
		});	
	
		let dir;

		if ($('#volver').val() == 2) {
			dir = 'administradores.php';
		}else if($('#volver').val() == 3){
			dir = 'tazadores.php';
		}

		$('#telefono').on('input', function () { 
		    this.value = this.value.replace(/[^0-9]/g,'');
		});

		$('#guardarTazador').on('click',function(){

			if ($('#nombre').val() == '' ||  $('#correo').val() == ''  || $('#telefono').val() == ''  || $('#clave').val() == '') {

		Swal.fire({
			title: 'Por favor complete los campos',
			type: 'info'
		});

			}else{
		dato = { 
			"nombre" : $('#nombre').val(),
			"correo" : $('#correo').val(),
			"telefono" : $('#telefono').val(),
			"clave" : $('#clave').val(),
			"status" : status
		};
			$.ajax({
			data: dato,
			url:'guardarEdicionTazador.php',
			method: "POST",
			success: function(res){ $("#init_content").html(res);

			$.ajax({
				url: dir,
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
	},
			error: function(err){   $("#init_content").html(err);}
		});
	}
	});

		$('#volver').on('click',function(){
			$.ajax({
				url : dir,
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
		});
</script>
