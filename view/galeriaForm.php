<div class="form-control">
    <div class="card">
        <div class="container-fluid flex-grow-1 container-p-y card-header">
          <h4 class="media align-items-center font-weight-bold  mb-0">
          <div class="media-body ml-3 col-12"><i class="fas fa-list"></i> Incluir galeria</div>
          </h4>
        </div>
      <div class="card-body">
          <form id="formulario" class="form-group" enctype="multipart/form-data" method="POST" action="saveGaleria.php">
                <div class="dz-message needsclick">Haga clic para subir</div>
                <span class="note needsclick">(Solo archivos de imagen <strong>no</strong> otro tipo.)</span>
                <div class="form-row">
                  <input name="codigo" type="text" readonly="readonly" value="<?=$codigo?>" hidden />
                  <input id="imagenes" name="imagenes[]" type="file" class="form-control" accept="image/png, .jpeg, .jpg, image/gif" multiple />
                </div>
                <br>
                <button id="guardarGaleria" type="submit" class="btn btn-primary">Guardar</button>
          </form>
      </div>
    </div>
</div>
<script src="assets/vendor/js/multifile.js"></script>