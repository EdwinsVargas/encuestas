
<div class="container-fluid flex-grow-1 container-p-y">
	<div class="card mb-4">
		<div class="container-fluid flex-grow-1 container-p-y card-header">
			<h4 class="media align-items-center font-weight-bold  mb-0">
			<div class="media-body ml-3 col-12"><i class="fas fa-list"></i> Nueva tazacion</div>
			</h4>
		</div>
		  <form class="card-body">
			  	<div class="form-row">
	                <div class="form-group col-lg-3">
		                <label class="form-label">Código</label>
		                <input required id="codeTazacion" type="text" class="form-control" value="<?=$taza['idtazaciones']?>"  readonly="readonly"  >
	                </div>
	                <div class="form-group col-lg-3">
		                <label class="form-label">Fecha de creacion</label>
		                <input required id="fecha" type="date" class="form-control" value="<?=$taza['fechaGeneracion']?>"  readonly="readonly"  >
	                </div>
				</div>
				<div class="form-row">
	                <div class="form-group col-lg-3">
		                  <label class="form-label">Municipio</label>
		                  <input id="municipio" name="municipio" class="form-control" value="<?=$taza['municipio']?>"  readonly="readonly" >
	                </div>
	                <div class="form-group col-lg-9">
		                <label class="form-label">Direccion</label>
		                <input id="direccion" required name="direccion" type="text" class="form-control" placeholder="Direccion mas espesifica"  value="<?=$taza['direccion']?>" >
	                </div>
	             </div>
	             <div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Nombre del contacto</label> 
		                <input id="cliente" required name="cliente" type="text" class="form-control" placeholder="Tu respuesta"  value="<?=$taza['cliente']?>" >
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Telefono de contacto</label> 
		                <input id="telefono" required name="telefono" type="tel" class="form-control" placeholder="Tu respuesta" value="<?=$taza['telefonoCliente']?>" >
	                </div>
				</div>
				<button id="guardarEdicionTazacion" type="button" class="btn btn-primary">Guardar cambios</button>
				<button id="volver" type="button" class="btn btn-default">Cancelar</button>
		  </form>
	</div>
</div>

<script>
	$('#estado').change(function(){
		dato = { "estado" : $('#estado').val()};
			$.ajax({
			data: dato,
			url:'ciudad.php',
			method: "POST",
			success: function(res){ $("#ciudad").html(res);},
			error: function(err){ $("#ciudad").html(err);}
		});
	});

	$('#ciudad').change(function(){
		dato = { "ciudad" : $('#ciudad').val()};
			$.ajax({
			data: dato,
			url:'municipio.php',
			method: "POST",
			success: function(res){ $("#municipio").html(res);},
			error: function(err){ $("#municipio").html(err);}
		});
	});
</script>


<script>

		$('#telefono').on('input', function () { 
		    this.value = this.value.replace(/[^0-9]/g,'');
		});

		$('#guardarEdicionTazacion').on('click',function(){

			if ($('#nombre').val() == '' ||  $('#fecha').val() == ''  || $('#direccion').val() == ''  || $('#cliente').val() == '' || $('#telefono').val() == '') {

		Swal.fire({
			title: 'Por favor complete los campos',
			type: 'info'
		});

			}else{

		dato = { 
			"codigo"    : $('#codeTazacion').val(),
			"fecha"     : $('#fecha').val(),
			"direccion" : $('#direccion').val(),
			"cliente"   : $('#cliente').val(),
			"municipio" : $('#municipio').val(),
			"telefono"  : $('#telefono').val()
		};

			$.ajax({
			data: dato,
			url:'updateSolicitud.php',
			method: "POST",
			success: function(res){ $("#init_content").html(res);

			$.ajax({
				url:'tazaciones.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
	},
			error: function(err){   $("#init_content").html(err);}
		});
	}
	});

		$('#volver').on('click',function(){
			$.ajax({
				url:'tazaciones.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
		});

</script>

