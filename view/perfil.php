<!DOCTYPE html>

<html lang="en" class="default-style">

<head>
  <title>Perfil Usuario</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <link rel="icon" type="image/x-icon" href="favicon.ico">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <!-- Icon fonts -->
  <link rel="stylesheet" href="assets/vendor/fonts/fontawesome.css">
  <link rel="stylesheet" href="assets/vendor/fonts/ionicons.css">
  <link rel="stylesheet" href="assets/vendor/fonts/linearicons.css">
  <link rel="stylesheet" href="assets/vendor/fonts/open-iconic.css">
  <link rel="stylesheet" href="assets/vendor/fonts/pe-icon-7-stroke.css">

  <!-- Core stylesheets -->
  <link rel="stylesheet" href="assets/vendor/css/rtl/bootstrap.css" class="theme-settings-bootstrap-css">
  <link rel="stylesheet" href="assets/vendor/css/rtl/appwork.css" class="theme-settings-appwork-css">
  <link rel="stylesheet" href="assets/vendor/css/rtl/theme-corporate.css" class="theme-settings-theme-css">
  <link rel="stylesheet" href="assets/vendor/css/rtl/colors.css" class="theme-settings-colors-css">
  <link rel="stylesheet" href="assets/vendor/css/rtl/uikit.css">
  <link rel="stylesheet" href="assets/css/demo.css">

  <script src="assets/vendor/js/material-ripple.js"></script>
  <script src="assets/vendor/js/layout-helpers.js"></script>

  <!-- Theme settings -->
  <!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
  <script src="assets/vendor/js/theme-settings.js"></script>


  <!-- Core scripts -->
  <script src="assets/vendor/js/pace.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Libs -->
  <link rel="stylesheet" href="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css">
  <!-- Page -->
  <link rel="stylesheet" href="assets/vendor/css/pages/users.css">
</head>

<body>
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-2">
    <div class="layout-inner">

      <!-- Layout container -->
      <div class="layout-container">

        <!-- Layout content -->
        <div class="layout-content">

          <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">

            <div class="media align-items-center py-3 mb-3">
              <div class="d-block ui-w-100 rounded-circle text-center">
                <h1>
                  <i class="fa fa-user"></i>
                </h1>
              </div>
              <div class="media-body ml-4">
                <h4 class="font-weight-bold mb-0"><?=$usuario['nombre']?>
                  <span class="text-muted font-weight-normal"><?=$usuario['correo']?></span>
                </h4>
                <div class="text-muted mb-2">Rol: <?=$rol?></div>
              </div>
            </div>

            <div class="card mb-4">
              <div class="card-body">
                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Nombre:</td>
                      <td><?=$usuario['nombre']?></td>
                    </tr>
                    <tr>
                      <td>Correo Electronico:</td>
                      <td><?=$usuario['correo']?></td>
                    </tr>
                    <tr>
                      <td>Compañia:</td>
                      <td>Tasaciones Inc.</td>
                    </tr>
                    <tr>
                      <td>Ordenes atendidas:</td>
                      <td><?=$ordenes_atendidas?></td>
                    </tr>
                    <tr>
                      <td>Verified:</td>
                      <td>
                        <span class="ion ion-md-checkmark text-primary"></span>&nbsp; Yes</td>
                    </tr>
                    <tr>
                      <td>Rol:</td>
                      <td><?=$rol?>
                	</td>
                    </tr>
                    <tr>
                      <td>Estatus:</td>
                      <td>
                        <span class="badge badge-outline-success">Active</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <hr class="border-light m-0">
              <div class="table-responsive">
                <table class="table card-table m-0">
                  <tbody>
                    <tr>
                      <th>Permisos</th>
                      <th>Generar</th>
                      <th>Resolver</th>
                      <th>Eliminar</th>
                      <th>Gastionar</th>
                    </tr>
                    <tr>
                      <td>Tasaciones</td>
                      <td>
                        <span class="ion ion-md-<?=$generar?> text-primary"></span>
                      </td>
                      <td>
                        <span class="ion ion-md-<?=$resolver?> text-primary"></span>
                      </td>
                      <td>
                        <span class="ion ion-md-<?=$eliminar?> text-primary"></span>
                      </td>
                      <td>
                        <span class="ion ion-md-<?=$gestionar?> text-primary"></span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <!-- <div class="card"> -->
              <!-- <div class="row no-gutters row-bordered">
                <div class="d-flex col-md align-items-center">
                  <a href="javascript:void(0)" class="card-body d-block text-dark">
                    <div class="text-muted small line-height-1">Tasaciones atendidas</div>
                    <div class="text-xlarge"><?=$ordenes_atendidas?></div>
                  </a>
                </div>
                <div class="d-flex col-md align-items-center">
                  <a href="javascript:void(0)" class="card-body d-block text-dark">
                    <div class="text-muted small line-height-1">Ordenes disponibles</div>
                    <div class="text-xlarge">534</div>
                  </a>
                </div>
                <div class="d-flex col-md align-items-center">
                  <a href="javascript:void(0)" class="card-body d-block text-dark">
                    <div class="text-muted small line-height-1">Following</div>
                    <div class="text-xlarge">236</div>
                  </a>
                </div>
              </div> -->
              <!-- <hr class="border-light m-0"> -->
              <!-- <div class="card-body"> -->


        <!--  <h6 class="mt-4 mb-3">Social links</h6>
               <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Twitter:</td>
                      <td>
                        <a href="javascript:void(0)">https://twitter.com/user</a>
                      </td>
                    </tr>
                    <tr>
                      <td>Facebook:</td>
                      <td>
                        <a href="javascript:void(0)">https://www.facebook.com/user</a>
                      </td>
                    </tr>
                    <tr>
                      <td>Instagram:</td>
                      <td>
                        <a href="javascript:void(0)">https://www.instagram.com/user</a>
                      </td>
                    </tr>
                  </tbody>
                </table> -->
              <!-- </div> -->
            </div>

          </div>
          <!-- / Content -->

        </div>
        <!-- Layout content -->

      </div>
      <!-- / Layout container -->

    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-sidenav-toggle"></div>
  </div>
  <!-- / Layout wrapper -->

  <!-- Libs -->
  <script src="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
