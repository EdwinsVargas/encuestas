
<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-6 col-lg-8">
            <h4  class="font-weight-bold mb-0 media-body mt-3 ml-3"><span><i class="fas fa-tasks"> </i> Tasaciones</span></h4>
          </div>
          <div class="col-12 col-sm-6 col-md-6 col-lg-4 d-flex float-right mt-3">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="ion ion-ios-search"></i>
                  </span>
                </div>
                <select id="estados" class="form-control">
                  <?php foreach ($estados as $estado): ?>
                    <option value="<?=$estado['idestado']?>"><?=$estado['nombre']?></option>
                  <?php endforeach ?>
                </select>
                <select id="ciudades" class="form-control"></select>
              </div>
          </div>
    </div>
</div>


<div class="container-fluid flex-grow-1 container-p-y">
      <section id="cards_cont" class="row" style="display: flex;">
<?php if (isset($tazaciones)) { ?>  
        <?php foreach ($tazaciones as $tazacion) { $IDTAZACION = $tazacion['idtazaciones']; ?>
                <div id="cartas" class="col-sm-12 col-md-6 col-lg-4 ">
                  <div class="card mb-3">
                    <div class="card-header">
                        <?php if ($tazacion['solved'] == '1'){ ?>
                        <span  style="padding: 5px 10px; width: 100px;" class="badge badge-success">Resuelto <i class="fas fa-check"></i></span>
                        <?php }else if ($tazacion['solved'] == '0'){ ?>
                        <span  style="padding: 5px 10px; width: 100px;" class="badge  badge-default">Sin resolver <i class="fas fa-pencil-alt"></i></span>
                        <?php } ?>
                    </div>
                    <div class="card-body">
                      <h4 class="card-title">Cliente <?=$tazacion['cliente']?></h4>
                      <p class="card-text"><i class="fas fa-phone"></i>         <strong>Teléfono: </strong><?=$tazacion['telefonoCliente']?></p>
                      <p class="card-text"><i class="fas fa-map-marker-alt"></i> <strong>Dirección: </strong><?=$tazacion['direccion'].'. '.$tazacion['municipio'].', '.$tazacion['ciudad'].' '.$tazacion['estado']?></p>
                      <p class="card-text"><i class="far fa-clock"></i>         <strong>Fecha de creación: </strong><?=$tazacion['fechaGeneracion']?></p>
                      <a href="javascript:void(0)" id="<?=$IDTAZACION?>" onclick="clicked('<?=$IDTAZACION?>')" data-datos='<?=$IDTAZACION?>' data-url='formulario.php' data-cont='init_content' class="btn btn-primary"><i class="fas fa-edit"></i> Atender orden</a>
                    </div>
                  </div>
                </div>
        <?php } ?>
<?php }else{echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasaciones registradas actualmente<h6></div>";} ?>
      </section>
</div>

<script src="assets/vendor/js/tazaciones.js"></script>
<script>

    $('#estados').on('change', function() {
        datos = {'estado': $('#estados').val()};
        $.ajax({
          data: datos,
          url:'ciudad.php',
          method: "POST",
          success: function(res){ $("#ciudades").html(res);},
          error: function(err){ $("#ciudades").html(err);}
        });
      });

    $('#ciudades').on('change', function() {
        datos = {'ciudad': $('#ciudades').val()};
        $.ajax({
          data: datos,
          url:'searchTazacionFiltered.php',
          method: "POST",
          success: function(res){ $("#cards_cont").html(res);},
          error: function(err){ $("#cards_cont").html(err);}
        });
      });
</script>