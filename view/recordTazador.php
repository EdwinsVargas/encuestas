<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row media align-items-center justify-content-center justify-content-sm-start">
          <div class="col-8 col-sm-8 col-md-9 col-lg-10 d-flex"><h4  class="font-weight-bold mb-0 media-body ml-3"><span class=""><i class="fas fa-history"> </i> Record de tasador</span></h4></div>
    </div>
</div>

<div class="container-fluid flex-grow-1 container-p-y">

      <div class="card">
              <h6 class="card-header" id="titulo">
                Record de <?=$codigo?>
              </h6>
              <div class="card-datatable table-responsive">

          <?php if (isset($records)) { ?>

                <table class="datatables-demo table table-striped ">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Código</th>
                      <th>Cliente</th>
                      <th>Contacto</th>
                      <th>Direccion</th>
                      <th>Fecha</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                    <tbody>
                     <?php foreach ($records as $record) { ?>
                      <tr class="odd gradeX">
                          <td><a title="Descargar reporte" download="Tasacion <?=$record['idtazaciones'].'.pdf'?>" href="assets/reportes/Tasacion <?=$record['idtazaciones'].'.pdf'?>" class="btn btn-info"> <i class="fas fa-file-pdf"></i></a></td>
                          <td style="vertical-align: baseline;" class="center"><?=$record['idtazaciones']?>   </td>
                          <td style="vertical-align: baseline;" class="center"><?=$record['cliente']?>        </td>
                          <td style="vertical-align: baseline;" class="center"><?=$record['telefonoCliente']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$record['direccion']?>. <?=$record['municipio']?>, <?=$record['ciudad']?> <?=$record['estado']?></td>
                          <td style="vertical-align: baseline;" class="center"><?=$record['fechaGeneracion']?> </td>
                          <td style="vertical-align: baseline;" class="center">
                            <?php if ($record['solved'] == '1'){ ?>
                              <span style="padding: 5px 10px; width: 90px;" class="badge badge-pill badge-success">Resuelto <i class="fas fa-check"></i></span>
                              <?php }else if ($record['solved'] == '0'){ ?>
                              <span style="padding: 5px 10px; width: 90px;" class="badge badge-pill badge-danger">Sin resolver   <i class="fas fa-user-slash"></i></span>
                            <?php } ?>
                          </td>
                      </tr>
                     <?php  } ?>
                  </tbody>
                </table>
              <?php }else{echo "<div class='col-md-12' style='text-align:center;'><h6>No hay tasaciones realizadas actualmente<h6></div>";} ?>
              </div>
            </div>
</div>

<script>
  $('.datatables-demo').dataTable();

  function eliminarTazacion($codigo){
      dato = { "codigo" : $codigo};
        $.ajax({
        data: dato,
        url:'eliminarTazacion.php',
        method: "POST",
        success: function(res){ $("#titulo").append(res);},
        error: function(err){ $("#titulo").append(err);}
      });

  }

  function editarTazacion($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        url:'editarTazacion.php',
        method: "POST",
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }

    function verReporte($codigo){
      dato = { "codigo" : $codigo};
      $.ajax({
        data: dato,
        url:'report.php',
        method: "POST",
        success: function(res){ $("#init_content").html(res);},
        error: function(err){ $("#initContent").html(err);}
      });
  }
</script>