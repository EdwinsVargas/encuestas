
<div class="container-fluid flex-grow-1 container-p-y">
	<div class="card mb-4">
		<div class="container-fluid flex-grow-1 container-p-y card-header">
			<h4 class="media align-items-center font-weight-bold  mb-0">
			<div class="media-body ml-3 col-12"><i class="fas fa-list"></i> Nueva tasación</div>
			</h4>
		</div>
		  <div class="card-body">
			  	<div class="form-row">
	                <div class="form-group col-lg-3">
		                <label class="form-label">Código</label>
		                <input id="codeTazacion"  type="text" class="form-control" value="<?=$codigo?>" readonly="readonly">
	                </div>
	                <div class="form-group col-lg-3">
		                <label class="form-label">Fecha de creación</label>
		                <input id="fecha" name="fecha" type="text" class="form-control" value="<?= $hoy['year'].'/'.$hoy['mon'].'/'.$hoy['mday']; ?>" readonly="readonly">
	                </div>
				</div>
				<div class="form-row">
	                <div class="form-group col-lg-3">
		                  <label class="form-label">Estado</label>
		                  <select id="estado" class="custom-select">
			                <?php foreach ($estados as $estado): ?><option value="<?=$estado['idestado']?>" id=""><?=$estado['nombre']?></option><?php endforeach ?>
		                  </select>
	                </div>
	               	<div class="form-group col-lg-3">
		                  <label class="form-label">Ciudad</label>
		                  <select id="ciudad" class="custom-select">
		                  </select>
	                </div>
	                <div class="form-group col-lg-3">
		                  <label class="form-label">Municipio</label>
		                  <select id="municipio" name="municipio" class="custom-select" >
		                  </select>
	                </div>
	                <div class="form-group col-lg-3">
		                <label class="form-label">Dirección</label>
		                <input id="direccion" name="direccion" type="text" class="form-control" placeholder="Direccion mas espesifica">
	                </div>
	             </div>
	             <div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Nombre del contacto</label> 
		                <input id="cliente" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Teléfono de contacto</label> 
		                <input id="telefono" type="tel" class="form-control" placeholder="Tu respuesta">
	                </div>
				</div>
				<button id="guardarTazacion" type="button" class="btn btn-primary">Guardar tasación</button>
				<button id="volver" type="button" class="btn btn-default">Volver</button>
		  </div>
	</div>
</div>

<script>
	$('#estado').change(function(){
		dato = { "estado" : $('#estado').val()};
			$.ajax({
			data: dato,
			url:'ciudad.php',
			method: "POST",
			success: function(res){ $("#ciudad").html(res);},
			error: function(err){ $("#ciudad").html(err);}
		});
	});

	$('#ciudad').change(function(){
		dato = { "ciudad" : $('#ciudad').val()};
			$.ajax({
			data: dato,
			url:'municipio.php',
			method: "POST",
			success: function(res){ $("#municipio").html(res);},
			error: function(err){ $("#municipio").html(err);}
		});
	});
</script>

<script>

		$('#telefono').on('input', function () { 
		    this.value = this.value.replace(/[^0-9]/g,'');
		});

		$('#guardarTazacion').on('click',function(){

			if ($('#nombre').val() == '' ||  $('#fecha').val() == ''  || $('#direccion').val() == ''  || $('#cliente').val() == '' || $('#telefono').val() == '') {

		Swal.fire({
			title: 'Por favor complete los campos',
			type: 'info'
		});

			}else{

		dato = { 
			"codigo"    : $('#codeTazacion').val(),
			"fecha"     : $('#fecha').val(),
			"direccion" : $('#direccion').val(),
			"cliente"   : $('#cliente').val(),
			"municipio" : $('#municipio').val(),
			"telefono"  : $('#telefono').val()
		};

			$.ajax({
			data: dato,
			url:'saveSolicitud.php',
			method: "POST",
			success: function(res){ $("#init_content").html(res);

			$.ajax({
				url:'tazaciones.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
	},
			error: function(err){   $("#init_content").html(err);}
		});
	}
	});

		$('#volver').on('click',function(){
			$.ajax({
				url:'tazaciones.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
		});
</script>