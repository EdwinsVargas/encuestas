
  <!-- Libs -->
  <link rel="stylesheet" href="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css">
  <link rel="stylesheet" href="assets/vendor/libs/datatables/datatables.css">

<div class="container-fluid flex-grow-1 container-p-y card-header">
    <div class="row media align-items-center justify-content-center justify-content-sm-start">
          <div class="col-8 col-sm-8 col-md-9 col-lg-10 d-flex"><h4  class="font-weight-bold mb-0 media-body ml-3"><span class=""><i class="fa fa-map-marker"> </i> Direcciones</span></h4></div>
    </div>
</div>

<div class="container-fluid flex-grow-1 container-p-y">

  		<div class="card">
          <h4 class="card-header">
                Registrar Estado
          </h4>
          <div class="card-body text-center">
          		<div class="row">
          			<div class="col-lg-2 col-md-2 col-sm-2">
          				<input id="codigoEstado" type="text" readonly="readonly" class="form-control" placeholder="Codigo" value="">
          			</div>
          			<div class="col-lg-6 col-md-5 col-sm-6">
          				<input id="nombreEstado" type="text" class="form-control" placeholder="Nombre del estado">
          			</div>
          			<div class="col-lg-4 col-md-2 col-sm-2">
          				<button id="comprobarIdEstado" class="btn btn-default"  style="display : none" >Comprobar ID</button>
          				<button id="guardar_estado" class="btn btn-primary"  style="display : none" >Guardar</button>
          			</div>
          		</div>
          		<br><br>
          		<div class="row" style="max-height: 500px; overflow-x: auto;">
	          		<?php if (isset($estados)): ?>          			
	          			<table class="table table-striped">
	          				<thead>
	          					<th></th>
	          					<th>Código</th>
	          					<th>Nombre</th>
	          				</thead>
	          				<tbody>
	          					<?php foreach ($estados as $estado): ?>
		          					<tr>
		          						<td><button onclick="eliminarEstado('<?=$estado['idestado']?>')" class="btn btn-primary"><i class="fa fa-trash"></i></button></td>
		          						<td><?=$estado['idestado']?></td>
		          						<td><?=$estado['nombre']?></td>
		          					</tr>
	          					<?php endforeach ?>
	          				</tbody>
	          			</table>
	          		<?php endif ?>
          		</div>
          </div>
        </div>
<br>
<br>
<br>
        <div class="card">
          <h4 class="card-header">
                Registrar Ciudad
          </h4>
          <div class="card-body text-center">
          		<div class="row">
          			<div class="col-lg-2 col-md-2 col-sm-2">
          				<input id="codigoCiudad" type="text" readonly="readonly" class="form-control" placeholder="Codigo" value="">
          			</div>
          			<div class="col-lg-2 col-md-5 col-sm-6">
          				<select id="est1" class="custom-select estados">
          					<option value=""></option>
          				</select>
          			</div>
          			<div class="col-lg-4 col-md-5 col-sm-6">
          				<input id="nombreCiudad" type="text" class="form-control" placeholder="Nombre de la ciudad">
          			</div>
          			<div class="col-lg-2 col-md-2 col-sm-2">
          				<button id="comprobarIdCiudad" class="btn btn-default"  style="display : none"  >Comprobar ID</button>
          				<button id="guardar_ciudad" class="btn btn-primary"  style="display : none"  >Guardar</button>
          			</div>
          		</div>
          		<br><br>
          		<div class="row" style="max-height: 500px; overflow-x: auto;">
	          		<?php if (isset($ciudades)): ?>          			
	          			<table class="datatables-demo table table-striped ">
	          				<thead>
	          					<th></th>
	          					<th>Código</th>
	          					<th>Nombre</th>
	          				</thead>
	          				<tbody>
	          					<?php foreach ($ciudades as $ciudad): ?>
		          					<tr>
		          						<td><button onclick="eliminarEstado('<?=$ciudad['idciudad']?>')" class="btn btn-primary"><i class="fa fa-trash"></i></button></td>
		          						<td><?=$ciudad['idciudad']?></td>
		          						<td><?=$ciudad['nombre']?></td>
		          					</tr>
	          					<?php endforeach ?>
	          				</tbody>
	          			</table>
	          		<?php endif ?>
          		</div>
          </div>
        </div>
<br>
<br>
<br>

        <div class="card">
          <h4 class="card-header">
                Registrar Municipio
          </h4>
          <div class="card-body text-center">
          		<div class="row">
          			<div class="col-lg-2 col-md-2 col-sm-2">
          				<input id="codigoMunicipio" type="text" readonly="readonly" class="form-control" placeholder="Codigo">
          			</div>

          			<div class="col-lg-2 col-md-5 col-sm-6">
          				<select id="est2" class="custom-select">
          					<option value="" selected hidden></option>
          				</select>
          			</div>

          			<div class="col-lg-2 col-md-5 col-sm-6">
          				<select id="ciu" class="custom-select">
          					<option value="" selected hidden></option>
          				</select>
          			</div>

          			<div class="col-lg-2 col-md-5 col-sm-6">
          				<input id="nombreMunicipio" type="text" class="form-control" placeholder="Nombre del municipio">
          			</div>
          			<div class="col-lg-4 col-md-2 col-sm-2">
          				<button id="comprobarIdMunicipio" class="btn btn-default" style="display : none">Comprobar ID</button>
          				<button id="guardar_municipio" class="btn btn-primary" style="display : none" >Guardar</button>
          			</div>
          		</div>	
          		<br><br>
          		<div class="row" style="max-height: 500px; overflow-x: auto;overflow-x: hidden;">
	          		<?php if (isset($municipios)): ?>          			
	          			<table class="table table-striped">
	          				<thead>
	          					<th></th>
	          					<th>Código</th>
	          					<th>Nombre</th>
	          				</thead>
	          				<tbody>
	          					<?php foreach ($municipios as $municipio): ?>
		          					<tr>
		          						<td><button onclick="eliminarMunicipio('<?=$municipio['idmunicipio']?>')" class="btn btn-primary"><i class="fa fa-trash"></i></button></td>
		          						<td><?=$municipio['idmunicipio']?></td>
		          						<td><?=$municipio['nombre']?></td>
		          					</tr>
	          					<?php endforeach ?>
	          				</tbody>
	          			</table> 
	          		<?php endif ?>
          		</div>
          </div>
        </div>
</div>
    <!-- Libs -->
	<script src="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
	<script src="assets/vendor/libs/datatables/datatables.js"></script>
	<script src="assets/vendor/js/direcciones.js"></script>
