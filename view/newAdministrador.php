
<div class="container-fluid flex-grow-1 container-p-y">
	<div class="card mb-4">
		<div class="container-fluid flex-grow-1 container-p-y card-header">
			<h4 class="media align-items-center font-weight-bold  mb-0">
			<div class="media-body ml-3 col-12"><i class="fas fa-list"></i> Nuevo Administrador</div>
			</h4>
		</div>
		  <form class="card-body" method="POST" action="">
	             <div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Nombre del administrador</label> 
		                <input id="nombre" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Correo electrónico</label>
		                <input id="correo" type="email" class="form-control" placeholder="Tu respuesta">
	                </div>
				</div>
				<div class="form-row">
	                <div class="form-group col-md-6">
		                <label class="form-label">Teléfono</label> 
		                <input  id="telefono" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                <label class="form-label">Clave</label> 
		                <input id="clave" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
				</div>
				<button id="guardarAdministrador" type="button" class="btn btn-primary">Guardar administrador</button>
				<button id="volver" type="button" class="btn btn-default">Volver</button>
		  </form>
	</div>
</div>

<script>

		$('#telefono').on('input', function () { 
		    this.value = this.value.replace(/[^0-9]/g,'');
		});

		$('#guardarAdministrador').on('click',function(){

			if ($('#nombre').val() == '' ||  $('#correo').val() == ''  || $('#telefono').val() == ''  || $('#clave').val() == '') {

		Swal.fire({
			title: 'Por favor complete los campos',
			type: 'info'
		});

			}else{

		dato = { 
			"nombre" : $('#nombre').val(),
			"correo" : $('#correo').val(),
			"telefono" : $('#telefono').val(),
			"clave" : $('#clave').val()
		};
			$.ajax({
			data: dato,
			url:'saveAdministrador.php',
			method: "POST",
			success: function(res){ $("#init_content").html(res);

			$.ajax({
				url:'administradores.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
	},
			error: function(err){   $("#init_content").html(err);}
		});
	}
	});
		$('#volver').on('click',function(){
			$.ajax({
				url:'administradores.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
		});
</script>

