<div class="container-fluid flex-grow-1 container-p-y">
	<div class="card mb-4">
	  <div class="card-header">
		    <h3>Formulario de Transcripción</h3>
		    <h7>Formulario utilizado para transcripción de información levantada manualmente</h7>
		    <br>
		    <h7 style="color: red;">* Obligartorio</h6>
	  </div>

	  <form class="card-body" action="saveForm.php" method="POST">
  			<div class="form-row">
                <div class="form-group col-md-6">
	                <label class="form-label">Código</label>
	                <input  required name="codigo" type="text" autocomplete='off' class="form-control" placeholder="Tu respuesta" value="<?=$_POST['codigo'] ?>"  readonly="readonly"  >
                </div>
                <div class="form-group col-md-6">
	                <label class="form-label">Fecha de inspección</label>
	                <input required name="fechaInspeccion" autocomplete='off' type="date" class="form-control" placeholder="Tu respuesta">
                </div>
          </div>
			<br>
	  			<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Fecha de Entrega(Corta- Ej. 14/06/1989)</label>
		                  <input required name="fechaEntregaCorta" autocomplete='off' type="date" class="form-control" placeholder="Tu respuesta" >
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Fecha de Entrega(Larga - Ej. 14 de junio de 1989)</label>
		                  <input required name="fechaEntregaLarga" autocomplete='off' type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	          </div>
	         <br>
	  			<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Nombre del Solicitante</label>
		                  <input required name="nombreSolicitante" autocomplete='off' type="text" class="form-control soloLetras" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Apellido del Solicitante</label>
		                  <input   required  name="apellidoSolicitante" autocomplete='off' type="text" class="form-control soloLetras" placeholder="Tu respuesta">
	                </div>
	          </div>
	         <br>
	  			<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Dirección del Proyecto</label>
		                  <input required name="direccionProyecto" autocomplete='off' type="text" class="form-control  soloLetras" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Sector</label>
		                  <input required  name="sector" autocomplete='off' type="text" class="form-control soloLetras" placeholder="Tu respuesta">
	                </div>
	          </div>
	          <br>
	  			<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Nro del Inmueble</label>
		                  <input name="numeroInmueble" autocomplete='off' required type="text" class="form-control soloNumeros" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Propietario(S)</label>
		                  <input   name="propietarios" autocomplete='off' required type="text" class="form-control soloLetras" placeholder="Tu respuesta">
	                </div>
	          </div>

	          <br>
	          <br>
	  			<div class="row">
                      <label class="col-form-label col-sm-2 text-sm-right pt-sm-0">Tipo de Documento Legal</label>
                      <div class="col-sm-10">
                        <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                            <input name="documentoLegal" type="radio" class="custom-control-input" value="Matricula"  checked>
                            <span class="custom-control-label">Matricula</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="documentoLegal" type="radio" class="custom-control-input" value="Certificado de Titulo">
                            <span class="custom-control-label">Certificado de Titulo</span>
                          </label>
                        </div>
                    </div>
                </div>
	          <br>
	          <br>
	  			<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Nro de Documento</label>
		                  <input required name="numeroDocumento" type="number" class="form-control soloNumeros" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Designación Catastral</label>
		                  <input required name="designacionCatastral" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	         	</div>
	         	<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Folio</label>
		                  <input required name="folio" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Libro</label>
		                  <input required name="libro" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	         	</div>
	         	<div class="form-row">
	                <div class="form-group col-md-4">
		                  <label class="form-label">Fecha de Expedición</label>
		                  <input required name="fechaExpedicion" type="date" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-4">
		                  <label class="form-label">Antigüedad de la Propiedad(años-Ej. 2010)</label>
		                  <input required name="antiguedadPropiedad" type="text" class="form-control soloNumeros" placeholder="Tu respuesta">
	                </div>
	         	
	                <div class="form-group col-md-4">
		                  <label class="form-label">Antigüedad Promedio del Entorno(años)</label>
		                  <input required name="antiguedadEntorno" type="text" class="form-control soloNumeros" placeholder="Tu respuesta">
	                </div>
				</div>
	         <br>
	          <br>
	  			<div class="row">
                      <label class="col-form-label col-sm-2 text-sm-right pt-sm-0">Ocupada Por</label>
                      <div class="col-sm-10">
                        <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                            <input name="ocupadaPor" type="radio" class="custom-control-input" value="Desocupada">
                            <span class="custom-control-label">Desocupada</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="ocupadaPor" type="radio" class="custom-control-input" value="Propietario"  checked>
                            <span class="custom-control-label">Propietario</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="ocupadaPor" type="radio" class="custom-control-input" value="Inquilino">
                            <span class="custom-control-label">Inquilino</span>
                          </label>
                        </div>
                    </div>
                </div>
	          <br>
	          <br>
	          <div class="row">
                      <label class="col-form-label col-sm-2 text-sm-right pt-sm-0">Tipo de Inmueble</label>
                      <div class="col-sm-10">
                        <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                            <input name="tipoInmueble" type="radio" class="custom-control-input" value="Apartamento" >
                            <span class="custom-control-label">Apartamento</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="tipoInmueble" type="radio" class="custom-control-input" value="Casa" checked>
                            <span class="custom-control-label">Casa</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="tipoInmueble" type="radio" class="custom-control-input" value="Solar">
                            <span class="custom-control-label">Solar</span>
                          </label>
                        </div>
                    </div>
                </div>
	          <br>
	          <br>

	          <div class="form-row">
	                <div class="form-group col-md-4">
		                  <label class="form-label">Área del Solar</label>
		                  <input required name="areaSolar" type="text" class="form-control" placeholder="Tu respuesta" required>
	                </div>
	                <div class="form-group col-md-4">
		                  <label class="form-label">Área de la Mejora</label>
		                  <input required name="areaMejora" type="text" class="form-control" placeholder="Tu respuesta" required>
	                </div>
	         	
	                <div class="form-group col-md-4">
		                  <label class="form-label">Lindero Norte *</label>
		                  <input required name="linderoNorte" type="text" class="form-control" placeholder="Tu respuesta" required>
	                </div>
				</div>
				<br>
				<div class="form-row">
	                <div class="form-group col-md-4">
		                  <label class="form-label">Lindero Sur *</label>
		                  <input required  name="linderoSur" type="text" class="form-control" placeholder="Tu respuesta" required>
	                </div>
	                <div   class="form-group col-md-4">
		                  <label class="form-label">Lindero Este *</label>
		                  <input required name="linderoEste" type="text" class="form-control is-invalid" placeholder="Esta respuesta es obligatoria" required>
	                </div>
	                <div class="form-group col-md-4">
		                  <label class="form-label">Lindero Oeste *</label>
		                  <input required name="linderoOeste" type="text" class="form-control" placeholder="Tu respuesta" required>
	                </div>
				</div>
				<br>
				<br>
				<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Distancia a Escuelas</label>
		                  <input required name="distanciaEscuelas" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>

	                <div class="form-group col-md-6">
		                  <label class="form-label">Distancia a Transporte Publico</label>
		                  <input required name="distanciaTransporte" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
				</div>
				<br>
				<br>
				<div class="form-row">
					<div class="form-group col-md-6">
		                  <label class="form-label">Distancia a Comercio</label>
		                  <input required name="distanciaComercio" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Distancia a Centro de la Ciudad</label>
		                  <input required name="distanciaCentroCiudad" type="text" class="form-control" placeholder="Tu respuesta">
	                </div>
				</div>
			<br>
			<br>
	          <div class="row">
                      <label class="col-form-label col-sm-2 text-sm-right pt-sm-0">Niveles Construidos - Inmueble</label>
                      <div class="col-sm-10">
                        <div class="custom-controls-stacked">
                          <label class="custom-control custom-radio">
                            <input name="nivelesConstruidosInmueble" type="radio" class="custom-control-input" value="1" checked>
                            <span class="custom-control-label">1</span>
                          </label>
                          <label class="custom-control custom-radio">
                            <input name="nivelesConstruidosInmueble" type="radio" class="custom-control-input" value="2">
                            <span class="custom-control-label">2</span>
                          </label>
                        </div>
                    </div>
                </div>
	          <br>
	          <br>
				<div class="form-row">
	                <div class="form-group col-md-6">
		                  <label class="form-label">Niveles Construidos - Estructura</label>
		                  <select name="nivelesConstruidosEstructura" class="custom-select">
			                  	<option value="1 " id="">1 </option>
			                  	<option value="2 " id="">2 </option>
			                  	<option value="3 " id="">3 </option>
			                  	<option value="4 " id="">4 </option>
			                  	<option value="5 " id="">5 </option>
			                  	<option value="6 " id="">6 </option>
			                  	<option value="7 " id="">7 </option>
			                  	<option value="8 " id="">8 </option>
			                  	<option value="9 " id="">9 </option>
			                  	<option value="10" id="">10</option>
			                  	<option value="11" id="">11</option>
			                  	<option value="12" id="">12</option>
			                  	<option value="13" id="">13</option>
			                  	<option value="14" id="">14</option>
			                  	<option value="15" id="">15</option>
			                  	<option value="16" id="">16</option>
			                  	<option value="17" id="">17</option>
			                  	<option value="18" id="">18</option>
			                  	<option value="19" id="">19</option>
			                  	<option value="20" id="">20</option>
			                  	<option value="21" id="">21</option>
			                  	<option value="22" id="">22</option>
			                  	<option value="23" id="">23</option>
			                  	<option value="24" id="">24</option>
			                  	<option value="25" id="">25</option>
			                  	<option value="26" id="">26</option>
			                  	<option value="27" id="">27</option>
			                  	<option value="28" id="">28</option>
			                  	<option value="29" id="">29</option>
			                  	<option value="30" id="">30</option>
		                  </select>
	                </div>
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Nivel donde se encuentra el inmueble</label>
		                  <select name="nivelInmueble" id="" class="custom-select">
			                  	<option value="1 " id="">1 </option>
			                  	<option value="2 " id="">2 </option>
			                  	<option value="3 " id="">3 </option>
			                  	<option value="4 " id="">4 </option>
			                  	<option value="5 " id="">5 </option>
			                  	<option value="6 " id="">6 </option>
			                  	<option value="7 " id="">7 </option>
			                  	<option value="8 " id="">8 </option>
			                  	<option value="9 " id="">9 </option>
			                  	<option value="10" id="">10</option>
			                  	<option value="11" id="">11</option>
			                  	<option value="12" id="">12</option>
			                  	<option value="13" id="">13</option>
			                  	<option value="14" id="">14</option>
			                  	<option value="15" id="">15</option>
			                  	<option value="16" id="">16</option>
			                  	<option value="17" id="">17</option>
			                  	<option value="18" id="">18</option>
			                  	<option value="19" id="">19</option>
			                  	<option value="20" id="">20</option>
			                  	<option value="21" id="">21</option>
			                  	<option value="22" id="">22</option>
			                  	<option value="23" id="">23</option>
			                  	<option value="24" id="">24</option>
			                  	<option value="25" id="">25</option>
			                  	<option value="26" id="">26</option>
			                  	<option value="27" id="">27</option>
			                  	<option value="28" id="">28</option>
			                  	<option value="29" id="">29</option>
			                  	<option value="30" id="">30</option>
		                  </select>
	                </div>
				</div>

			<br>
			<br>
			<br>
				<div class="card-header">
				    <h3>Ambientes</h3>
		  		</div>
		  	<div class="form-row">
	            <table class="table table-striped">
	            	<thead>
            			<th></th>
            			<th>1</th>
            			<th>2</th>
            			<th>3</th>
	            	</thead>
              <tbody>
                <tr>
                  <th scope="row">Entrada / Lobby</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="entradaLobby" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="entradaLobby" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="entradaLobby" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Marquesina</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="marquesina" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="marquesina" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="marquesina" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Galeria</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="galeria" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="galeria" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="galeria" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Recibidor</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="recibidor" type="radio" class="custom-control-input" value="1"  checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="recibidor" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="recibidor" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Sala</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="sala" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="sala" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="sala" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Comedor</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="comedor" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="comedor" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="comedor" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Cocina</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocina" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocina" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocina" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Cocina Fría / Cocina Caliente</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaFriaCaliente" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaFriaCaliente" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaFriaCaliente" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Cocina con Desayunador</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaDesayunador" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaDesayunador" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaDesayunador" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Cocina con Isla</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaIsla" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaIsla" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cocinaIsla" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">1/2 Baño</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="bano" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="bano" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="bano" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Sala de Estar</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="salaEstar" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="salaEstar" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="salaEstar" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Área Lavado</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="areaLavado" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="areaLavado" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="areaLavado" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Cuarto de Servicio</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cuartoServicio" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cuartoServicio" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="cuartoServicio" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Balcón</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="balcon" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="balcon" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="balcon" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Terraza</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="terraza" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="terraza" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="terraza" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Patio</th>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="patio" type="radio" class="custom-control-input" value="1" checked>
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="patio" type="radio" class="custom-control-input" value="2">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                  <td>
                  	<label class="custom-control custom-radio">
	                    <input name="patio" type="radio" class="custom-control-input" value="3">
	                    <span class="custom-control-label"></span>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
			</div>

<br><br><br>

				<div class="card-header">
				    <h3>Dormitorios</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>1</th>
		  					<th>2</th>
		  					<th>3</th>
		  					<th>4</th>
		  				</thead>
		  				<tbody>

		  			
		  					<tr>
		  						<td>Grandes</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosGrandes" type="radio" class="custom-control-input" value="1"  checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosGrandes" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosGrandes" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosGrandes" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Medianos</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosMedianos" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosMedianos" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosMedianos" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosMedianos" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Pequeños</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosPequenos" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosPequenos" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosPequenos" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="dormitoriosPequenos" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>

		  				</tbody>
		  			</table>
		  		</div>
<br><br><br>

				<div class="card-header">
				    <h3>Walking Closets</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>1</th>
		  					<th>2</th>
		  					<th>3</th>
		  					<th>4</th>
		  				</thead>
		  				<tbody>
		  					<tr>
		  						<td>Primer Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel1" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel1" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel1" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel1" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Segundo Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel2" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel2" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel2" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="wClosetNivel2" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  				</tbody>
		  			</table>
		  		</div>

		<br><br><br>
				<div class="card-header">
				    <h3>Closets</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>1</th>
		  					<th>2</th>
		  					<th>3</th>
		  					<th>4</th>
		  				</thead>
		  				<tbody>
		  					<tr>
		  						<td>Primer Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel1" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel1" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel1" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel1" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Segundo Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel2" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel2" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel2" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel2" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Tercer Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel3" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel3" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel3" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="closetNivel3" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  				</tbody>
		  			</table>
		  		</div>

<br><br><br>
				<div class="card-header">
				    <h3>Baños</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>1</th>
		  					<th>2</th>
		  					<th>3</th>
		  					<th>4</th>
		  				</thead>
		  				<tbody>
		  					<tr>
		  						<td>Primer Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel1" type="radio" class="custom-control-input" value="1"  checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel1" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel1" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel1" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Segundo Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel2" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel2" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel2" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel2" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Tercer Nivel</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel3" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel3" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel3" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="banoNivel3" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  				</tbody>
		  			</table>
		  		</div>

				<br><br><br>
				<div class="card-header">
				    <h3>Pisos</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>Primer Nivel</th>
		  					<th>Segundo Nivel</th>
		  				</thead>
		  				<tbody>
		  					<tr>
		  						<td>
		  							Cerámica
		                      	</td>

		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Cerámica" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Cerámica" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
							</tr>

							<tr>
								<td>
		  							Porcelanato
		                      	</td>									
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Porcelanato">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>

		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Porcelanato">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
							</tr>

							<tr>
								<td>
		  							Granito
		                      	</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Granito">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Granito">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
                            </tr>

                            <tr>
	                            <td>
	                            	Mármol
	                            </td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Mármol">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Mármol">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
                            </tr>
          
                            <tr>
                            	<td>
                            		Parquet
                            	</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Parquet">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Parquet">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
                            </tr>

							 <tr>
                            	<td>
                            		Cemento Pulido
                            	</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel1" type="radio" class="custom-control-input" value="Cemento Pulido">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="pisoNivel2" type="radio" class="custom-control-input" value="Cemento Pulido">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
                            </tr>
		  				</tbody>
		  			</table>
		  		</div>
<br>
<br>
<br>
				<div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Ventana</label>
		                  <select name="ventana" class="custom-select">
			                  	<option value="NA" id="">NA</option>
			                  	<option value="Corredisas" id="">Corredizas de Aluminio y Vidrio</option>
			                  	<option value="Fondo" id="">Frente Corredizas de aluminio y Vidrio / Celosías al Fondo</option>
			                  	<option value="celocias" id="">Celosias</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">
						<label class="form-label">Puerta Principal</label>
		                  <select name="puertaPrincipal"  class="custom-select">
			                  	<option value="Caoba" >Caoba</option>
			                  	<option value="Roble" >Roble</option>
			                  	<option value="Cedro" >Cedro</option>
			                  	<option value="Pino" >Pino</option>
			                  	<option value="Andiroba" >Andiroba</option>
			                  	<option value="Everdor" >Everdor</option>
			                  	<option value="Plywood" >Plywood</option>
			                  	<option value="Madera Preciosa" >Madera Preciosa</option>
		                  </select>
	                </div>
				</div>
			<br>
			<br>
			<br>
				<div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Puertas Interiores</label>
		                  <select name="puertasInteriores" class="custom-select">
								<option value="NA">NA</option>
								<option value="Caoba">Caoba</option>
								<option value="Roble">Roble</option>
								<option value="Cedro">Cedro</option>
								<option value="Pino">Pino</option>
								<option value="Andiroba">Andiroba</option>
								<option value="Everdor">Everdor</option>
								<option value="Plywood">Plywood</option>
								<option value="Madera Prensada">Madera Prensada</option>
								<option value="Madera Preciosa">Madera Preciosa</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">
						<label class="form-label">Closets</label>
		                  <select name="closets" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Caoba">Caoba</option>
			                  	<option value="Roble">Roble</option>
			                  	<option value="Cedro">Cedro</option>
			                  	<option value="Pino">Pino</option>
			                  	<option value="Andiroba">Andiroba</option>
			                  	<option value="Everdoor">Everdoor</option>
			                  	<option value="Plywood">Plywood</option>
			                  	<option value="Aluminio_y_Espejo">Puertas Corredizas de Aluminio y Espejo</option>
			                  	<option value="Madera_Prensada">Madera Prensada</option>
		                  </select>
	                </div>
				</div>
			<br>
			<br>
			<br>
				<div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Walking Closets</label>
		                  <select name="wClosets" id="" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Caoba">Caoba</option>
			                  	<option value="Roble">Roble</option>
			                  	<option value="Cedro">Cedro</option>
			                  	<option value="Pino">Pino</option>
			                  	<option value="Andiroba">Andiroba</option>
			                  	<option value="Everdoor">Everdoor</option>
			                  	<option value="Plywood">Plywood</option>
			                  	<option value="Puertas Corredizas de Aluminio y Espejo">Puertas Corredizas de Aluminio y Espejo</option>
			                  	<option value="Madera Prensada">Madera Prensada</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">
						<label class="form-label">Gabinetes de Cocina</label>
		                  <select name="gabinetesCocina" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Caoba">Caoba</option>
			                  	<option value="Roble">Roble</option>
			                  	<option value="Cedro">Cedro</option>
			                  	<option value="Pino">Pino</option>
			                  	<option value="Andiroba">Andiroba</option>
			                  	<option value="Everdoor">Everdoor</option>
			                  	<option value="Plywood">Plywood</option>
			                  	<option value="Madera_Prensada">Madera Prensada</option>
		                  </select>
	                </div>
				</div>
			<br>
			<br>
			<br>
				<div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Tope de Cocina</label>
		                  <select name="topeCocina" id="" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Granito">Granito</option>
			                  	<option value="Ceramica">Cerámica</option>
			                  	<option value="Marmolite">Marmolite</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">
						<label class="form-label">Pared de Cocina</label>
		                  <select name="paredCocina" id="" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Granito">Granito</option>
			                  	<option value="Ceramica">Cerámica</option>
		                  </select>
	                </div>
				</div>
			<br>
			<br>
			<br>
				<div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Baños</label>
		                  <select name="banos" id="" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Mampara">Mampara</option>
			                  	<option value="Bañera">Bañera</option>
			                  	<option value="Puertas Corredizas">Puertas Corredizas</option>
			                  	<option value="Puertas Corredizas y Bañera">Puertas Corredizas y Bañera</option>
			                  	<option value="Mixtos">Mixtos</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">
						<label class="form-label">Revestimiento en Baños</label>
		                  <select name="revestimientoBanos" class="custom-select">
			                  	<option value="NA">NA</option>
			                  	<option value="Cerámica">Cerámica</option>
			                  	<option value="Mármol">Mármol</option>
		                  </select>
	                </div>
				</div>
			<br>
			<br>
			<br>
			<div class="card-header">
				    <h3>Parqueos</h3>
		  		</div>

		  		<div class="form-row">
		  			<table class="table table-striped">
		  				<thead>
		  					<th></th>
		  					<th>1</th>
		  					<th>2</th>
		  					<th>3</th>
		  					<th>4</th>
		  				</thead>
		  				<tbody>
		  					<tr>
		  						<td>Aire Libre</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosAireLibre" type="radio" class="custom-control-input" value="1"  checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosAireLibre" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosAireLibre" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosAireLibre" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  					<tr>
		  						<td>Techado</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosTechado" type="radio" class="custom-control-input" value="1" checked>
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosTechado" type="radio" class="custom-control-input" value="2">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosTechado" type="radio" class="custom-control-input" value="3">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  						<td>
									<label class="custom-control custom-radio">
		                            <input name="parqueosTechado" type="radio" class="custom-control-input" value="4">
		                            <span class="custom-control-label"></span>
		                          </label>
		  						</td>
		  					</tr>
		  				</tbody>
		  			</table>
		  		</div>
		  		 <br>
		  		 <br>
		  		 <br>
		  		 <div class="form-row">
	               	<div class="form-group col-md-6">
		                  <label class="form-label">Techos</label>
		                  <select name="techos" class="custom-select">
								<option value="NA" >NA</option>
			                  	<option value="Pañete, Pintura y Cornisas de Yeso">Pañete, Pintura y Cornisas de Yeso</option>
								<option value="Sheet Rock" >Sheet Rock</option>
		                  </select>
	                </div>
	                <div class="form-group col-md-6">


	                	<label class="form-label">Condiciones Generales del Inmueble</label>
		                  <select name="condicionesGeneralesInmueble" class="custom-select">
								<option value="buenas condiciones">buenas condiciones</option>
			                  	<option value="malas condiciones">malas condiciones</option>
			                  	<option value="condiciones habitables">condiciones habitables</option>
			                  	<option value="condiciones habitables, sin embargo, requiere mantenimiento en general">condiciones habitables, sin embargo, requiere mantenimiento en general</option>
			                  	<option value="condiciones regulares">condiciones regulares</option>
			                  	<option value="buenas condiciones, el mismo es nuevo y se encuentra listo para entrega">buenas condiciones, el mismo es nuevo y se encuentra listo para entrega</option>
			                  	<option value="buenas condiciones, el mismo es nuevo y se encuentra en fase de terminación">buenas condiciones, el mismo es nuevo y se encuentra en fase de terminación</option>
		                  </select>

	                	</div>
				</div>
				<br>
		  		<br>
		  		<br>
		  		 <br>
		  		 <div class="form-row">
	               	<div class="form-group col-md-6">
		                  <div class="card-header">
								<h3>Amenidades</h3>
		                  	</div>
						<br>

			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[plantaFull]" value="SI"><span class="custom-control-label">planta full</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[plantaAreasComunes]" value="SI"><span class="custom-control-label">planta áreas comunes</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[ascensor]" value="SI"><span class="custom-control-label">ascensor</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[Piscina]" value="SI"><span class="custom-control-label">piscina</span></label>
							<label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[picuzzi]" value="SI"><span class="custom-control-label">picuzzi</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[jacuzzi]" value="SI"><span class="custom-control-label">jacuzzi</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[salonReuniones]" value="SI"><span class="custom-control-label">salón de reuniones</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[gazebo]" value="SI"><span class="custom-control-label">gazebo</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[areaJuegos]" value="SI"><span class="custom-control-label">área de juegos para niños</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[gimnacio]" value="SI"><span class="custom-control-label">gimnasio</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[terraza]" value="SI"><span class="custom-control-label">terraza</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[terrazaBar]" value="SI"><span class="custom-control-label">terraza con bar</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[pozo]" value="SI"><span class="custom-control-label">pozo</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[cisterna]" value="SI"><span class="custom-control-label">cisterna</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[portonElectrico]" value="SI"><span class="custom-control-label">portón eléctrico</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[gasTuberia]" value="SI"><span class="custom-control-label">gas por tubería</span></label>
			                <label class="custom-control custom-checkbox m-0" ><input class="custom-control-input" type="checkbox" name="amenidades[intercom]" value="SI"><span class="custom-control-label">intercom</span></label>
	                
	                </div>
	                <div class="form-group col-md-6">
	                		<div class="card-header">
								<h3>Adicionales</h3>
		                  	</div>
		                  	<br>
		                  		<label class="custom-control custom-checkbox m-0"><input class="custom-control-input" type="checkbox" name="adicionales[herrajePuertaPrincipal]" value="SI"><span class="custom-control-label">herraje en puerta principal</span></label>
		                  		<label class="custom-control custom-checkbox m-0"><input class="custom-control-input" type="checkbox" name="adicionales[herrajeVentanas]" value="SI"><span class="custom-control-label">herraje en ventanas</span></label>
								<label class="custom-control custom-checkbox m-0"><input class="custom-control-input" type="checkbox" name="adicionales[aaCentral]" value="SI"><span class="custom-control-label">A/A central</span></label>
								<label class="custom-control custom-checkbox m-0"><input class="custom-control-input" type="checkbox" name="adicionales[shuttersVentanas]" value="SI"><span class="custom-control-label">shutters en ventanas</span></label>
								<label class="custom-control custom-checkbox m-0"><input class="custom-control-input" type="checkbox" name="adicionales[shuttersBalcon]" value="SI"><span class="custom-control-label">shutters en balcón</span></label>
	                </div>
				</div>
				<br>
		  		<br>
		  		<br>
		  		 <div class="form-row">
	               	<div class="form-group col-md-12">
							<div class="card-header">
								<h3>Comentarios Generales sobre la Propiedad</h3>
								<h7>La zona es residencial de clase media, presenta desarrollo sostenido acorde a su mejor y mas alto uso</h7>
								<h7>en su cercanía existen variedad de negocios, tales como:</h7>
		                  	</div>
		                  <br>
		                  <br>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="plazas comerciales"><span class="custom-control-label">plazas comerciales</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="farmacia">				<span class="custom-control-label">farmacia</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="estación de combustible">	<span class="custom-control-label">estación de combustible</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="supermercado">			<span class="custom-control-label">supermercado</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="restaurantes">			<span class="custom-control-label">restaurantes</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="gimnasio">				<span class="custom-control-label">gimnasio</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="estación de metro">		<span class="custom-control-label">estación de metro</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="clinicas">				<span class="custom-control-label">clinicas</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="hospital público">		<span class="custom-control-label">hospital público</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="el sector cuenta con calles asfaltadas con aceras y contenes">		<span class="custom-control-label">el sector cuenta con calles asfaltadas con aceras y contenes</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="el sector cuenta con calles y parqueos en hormigón armado">			<span class="custom-control-label">el sector cuenta con calles y parqueos en hormigón armado</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="el sector no cuenta con calles asfaltadas, con aceras y contenes">	<span class="custom-control-label">el sector no cuenta con calles asfaltadas, con aceras y contenes</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="el sector no cuenta con calles asfaltadas, ni con aceras ni contenes"><span class="custom-control-label">el sector no cuenta con calles asfaltadas, ni con aceras ni contenes</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="tráfico tranquilo en horas picos">									<span class="custom-control-label">tráfico tranquilo en horas picos</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="tráfico complicado">													<span class="custom-control-label">tráfico complicado</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="cuentan con recogida de basura">										<span class="custom-control-label">cuentan con recogida de basura</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="no cuentan con recogida de basura">									<span class="custom-control-label">no cuentan con recogida de basura</span></label>
	                  		<label class="custom-control custom-checkbox m-0"> <input  class="custom-control-input" type="checkbox" name="negocios[]" value="servicios de telefonía, internet, televisión por cable y agua potable por tubería"><span class="custom-control-label">servicios de telefonía, internet, televisión por cable y agua potable por tubería</span></label>
	                </div>
				</div>
				<br>
		  		<br>
		  		<div class="form-row">
	             <div class="form-group col-md-6">
		            <label class="form-label">Coordenadas Geográficas</label>
		            <div class="input-group">
		                <button class="btn input-group-prepend" type="button" id="obtenerGeo"><i class="ion ion-ios-search"></i></button>
		                <input type="text" required name="coordenadasGeo" id="coordenadasGeo" class="form-control" placeholder="_ _ _ , _ _ _ " readonly>
	              </div>
	                </div>
	                <div class="form-group col-md-6">
		                  <label class="form-label">Observaciones</label>
		                  <textarea maxlength="400" name="observaciones" required class="form-control soloLetras" placeholder="Tu respuesta"></textarea>
	                </div>
	         	</div>
	         	<div class="form-group row">
                    <div class="col-sm-12 ml-sm-auto">
                      <button type="submit" class="btn btn-primary">Gruardar</button>
                      <button id="volver" type="button" class="btn btn-default">Volver</button>
                    </div>
                  </div>
	  </form>
	</div>
</div>

<script>


		$('#volver').on('click',function(){
			$.ajax({
				url:'tazaciones_tazador.php',
				success: function(res){ $("#init_content").html(res);},
				error: function(err){   $("#init_content").html(err);}
			});
		});
// *************************************************
		$('#obtenerGeo').click(function(){
			getLocation();
		});

		function getLocation() {
			if (navigator.geolocation) {
            	navigator.geolocation.getCurrentPosition(showPosition);
            	} else {
            	    console.log("Geolocation is not supported by this browser.");	
            	}
    	}
		function showPosition(position) {
				$('#coordenadasGeo').val(position.coords.latitude+','+position.coords.longitude);
		}

		$('.soloLetras').keypress(function (tecla) {
			if ((tecla.charCode > 32 && tecla.charCode < 65)||(tecla.charCode > 90 && tecla.charCode < 97)||(tecla.charCode > 122)){ return false; }
		});

		jQuery('.soloNumeros').keypress(function (tecla) {
		  if (tecla.charCode < 48 || tecla.charCode > 57) return false;
		});
</script>
