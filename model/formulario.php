<?php


class formulario{

	private $idUsuario;
	private $idFormulario;
	private $bd;
	private $newCodigo;

	private $idBasics;
	private $idAmbients;
	private $idAmenidades;
	private $idBanos;
	private $idAdicionales;
	private $idWalkingClosets;
	private $idPisos;
	private $idClosets;
	private $idParqueos;
	private $idDormitorios;
	private $idUsers;

	function __construct($usuario,$codigo){

		require 'conexion.php';

		$this->bd = conexion::conectar();

		$this->idUsuario=$usuario;
		$this->idFormulario=$codigo;
	}


	public function newBasicos(
$fechaInspeccion,$fechaEntregaCorta,
$fechaEntregaLarga,$nombreSolicitante,
$apellidoSolicitante,$direccionProyecto,
$numeroInmueble,$sector,
$propietarios,$documentoLegal,
$numeroDocumento,$designacionCatastral,
$folio,$libro,$fechaExpedicion,
$antiguedadPropiedad,$antiguedadEntorno,
$ocupadaPor,$tipoInmueble,$areaSolar,
$areaMejora,$linderoNorte,$linderoSur,
$linderoEste,$linderoOeste,$distanciaEscuelas,
$distanciaTransporte,$distanciaComercio,$distanciaCentroCiudad,
$nivelesConstruidosInmueble,$nivelesConstruidosEstructura,
$nivelInmueble,$ventana,$puertaPrincipal,$puertasInteriores,
$closets,$wClosets,$gabinetesCocina,$topeCocina,$paredCocina,
$banos,$revestimientoBanos,$techos,$condicionesGeneralesInmueble,
$coordenadasGeo,$observaciones,$nombreBasic = 'Basic'
){

	$this->idBasics = $this->verificacionDuplicados('basicos','idbasicos',$nombreBasic);

$this->bd->query("INSERT INTO `basicos`(`idbasicos`, `fechaInspeccion`, `fechaEntregaCorta`, `fechaEntregaLarga`, `nombreSolisitante`, `apellidoSolicitante`, `direccionProyecto`, `numeroInmueble`, `sector`, `propietarios`, `tipoDocumentoLegal`, `numeroDocumento`, `designacionCatastral`, `folio`, `libro`, `fechaExpedicion`, `antigueadadPropiedad`, `antiguedadEntorno`, `ocupadaPor`, `tipoInmueble`, `areaSolar`, `areaMejora`, `linderoNorte`, `linderoSur`, `linderoEste`, `linderoOeste`, `distanciaEscuelas`, `distanciaTransportePublico`, `distanciaComercio`, `distanciaCentroCiudad`, `nivelesConstruidosInmueble`, `nivelesConstruidosEstructura`, `nivelInmueble`, `ventana`, `puertaPrincipal`, `puertasInteriores`, `closets`, `walkingClosets`, `gabinetesCocina`, `topeCocina`, `paredCocina`, `banos`, `revestimientoBanos`, `techos`, `condicionesGeneralesInmueble`, `coordenadasGeo`, `observaciones`) VALUES (

'".$this->idBasics."',
'".$fechaInspeccion."', '".$fechaEntregaCorta."', 
'".$fechaEntregaLarga."', '".$nombreSolicitante."', 
'".$apellidoSolicitante."', '".$direccionProyecto."', 
'".$numeroInmueble."', '".$sector."', 
'".$propietarios."', '".$documentoLegal."', 
'".$numeroDocumento."', '".$designacionCatastral."', 
'".$folio."', '".$libro."', 
'".$fechaExpedicion."', '".$antiguedadPropiedad."', 
'".$antiguedadEntorno."', '".$ocupadaPor."', 
'".$tipoInmueble."', '".$areaSolar."', 
'".$areaMejora."', '".$linderoNorte."', 
'".$linderoSur."', '".$linderoEste."', 
'".$linderoOeste."', '".$distanciaEscuelas."', 
'".$distanciaTransporte."', '".$distanciaComercio."', 
'".$distanciaCentroCiudad."', '".$nivelesConstruidosInmueble."', 
'".$nivelesConstruidosEstructura."', '".$nivelInmueble."', 
'".$ventana."', '".$puertaPrincipal."', 
'".$puertasInteriores."', '".$closets."', 
'".$wClosets."', '".$gabinetesCocina."', 
'".$topeCocina."', '".$paredCocina."', 
'".$banos."', '".$revestimientoBanos."', 
'".$techos."', 
'".$condicionesGeneralesInmueble."', '".$coordenadasGeo."', 
'".$observaciones."'
);");

}

	// ***********************************************************************************
	public function newAmbientes($entradaLobby,$marquesina,$galeria,$recibidor,$sala,$comedor,$cocina,$cocinaFriaCaliente,$cocinaDesayunador,$cocinaIsla,$bano,$salaEstar,$areaLavado,$cuartoServicio,$balcon,$terraza,$patio,$nombreAmbient='ambient'){
		
		$this->idAmbients = $this->verificacionDuplicados('ambientes','idambientes',$nombreAmbient);

		$this->bd->query("INSERT INTO `ambientes` (`idambientes`,`entradaLobby`,`marquesina`,`galeria`,`recibidor`,`sala`,`comedor`,
			`cocina`,`cocinaFriaYCaliente`,	`cocinaDesayunador`,`cocinaIsla`,`bano`,`salaEstar`,`areaLavado`,`cuartoServicio`,`balcon`,
			`terraza`,`patio`
		) VALUES (
			'".$this->idAmbients."',
			'".$entradaLobby."',
			'".$marquesina."',
			'".$galeria."',
			'".$recibidor."',
			'".$sala."',
			'".$comedor."',
			'".$cocina."',
			'".$cocinaFriaCaliente."',
			'".$cocinaDesayunador."',
			'".$cocinaIsla."',
			'".$bano."',
			'".$salaEstar."',
			'".$areaLavado."',
			'".$cuartoServicio."',
			'".$balcon."',
			'".$terraza."',
			'".$patio."'
		)");
	}

	// ***********************************************************************************
	public function newAmenidades($datos,$nombreAmenidades="amenidades"){

		$this->idAmenidades =  $this->verificacionDuplicados("amenidades", "idamenidades",$nombreAmenidades);
		$columnas= "";
		$valores= "";
		foreach ($datos as $key => $dato) {
			$columnas.=", `".$key."`";
			$valores.=", '".$dato."'";
		}
		$this->bd->query("INSERT INTO `amenidades` (`idamenidades`".$columnas.") VALUES ('".$this->idAmenidades."'".$valores.");");
	}

	// ***********************************************************************************
	public function newBanos($primerNivel,$segundoNivel,$tercerNivel,$nombreBanos='banos'){

		$this->idBanos =  $this->verificacionDuplicados("banos", "idbanos",$nombreBanos);

		$this->bd->query("INSERT INTO `banos` (
			`idbanos`, 
			`primerNivel`, 
			`segundoNivel`, 
			`tercerNivel`) VALUES (
			'".$this->idBanos."', 
			'".$primerNivel."', 
			'".$segundoNivel."', 
			'".$tercerNivel."');"
		);
	}

	// ***********************************************************************************
	public function newAdicionales($datos,$nombreAdicionales='add'){
		
		$this->idAdicionales =  $this->verificacionDuplicados("adicionales", "idadicionales",$nombreAdicionales);

		$columnas= "";
		$valores = "";

		foreach ($datos as $key => $dato) {
			$columnas.=", `".$key."`";
			$valores .=", '".$dato."'";
		}

		$this->bd->query("INSERT INTO `adicionales` (`idadicionales`".$columnas.") VALUES ('".$this->idAdicionales."'".$valores.");");
	}

	// ***********************************************************************************
	public function newWalkingCloset($primerNivel,$segundoNivel,$nombreWalkingClosets='wc'){

		$this->idWalkingClosets =  $this->verificacionDuplicados("walkingclosets", "idwalkingClosets",$nombreWalkingClosets);

		$this->bd->query("INSERT INTO `walkingclosets` (
			`idwalkingClosets`, 
			`primerNivel`, 
			`segundoNivel`
		) VALUES ('".$this->idWalkingClosets."','".$primerNivel."', '".$segundoNivel."');");
	}

	// ***********************************************************************************
	public function newPisos($primerNivel,$segundoNivel,$nombrePisos='pisos'){

		$this->idPisos =  $this->verificacionDuplicados("pisos", "idpisos",$nombrePisos);

		$this->bd->query("INSERT INTO `pisos` (`idpisos`, `primerNivel`, `SegundoNivel`) VALUES ('".$this->idPisos."', '".$primerNivel."', '".$segundoNivel."');");
	}

	// ***********************************************************************************
	public function newClosets($primerNivel,$segundoNivel,$tercerNivel,$nombreClosets='closets'){

		$this->idClosets =  $this->verificacionDuplicados("closets", "idclosets",$nombreClosets);

		$this->bd->query("INSERT INTO `closets` (`idclosets`, `primerNivel`, `SegundoNivel`, `tercerNivel`) VALUES ('".$this->idClosets."', '".$primerNivel."', '".$segundoNivel."', '".$tercerNivel."');");
	}

	// ***********************************************************************************
	public function newParqueos($parqueosAireLibre,$parqueosTechado,$nombreParqueo='parqueo'){

		$this->idParqueos =  $this->verificacionDuplicados("parqueos", "idparqueos",$nombreParqueo);
		
		$this->bd->query("INSERT INTO `parqueos` (`idparqueos`, `aireLibre`, `techado`) VALUES ('".$this->idParqueos."', '".$parqueosAireLibre."', '".$parqueosTechado."');");
	}

	// ***********************************************************************************
	public function newDormitorios($grandes,$medianos,$pequenos,$nombreDormitorio='dorm'){

		$this->idDormitorios =  $this->verificacionDuplicados("dormitorios", "iddormitorios",$nombreDormitorio);
		
		$this->bd->query("INSERT INTO `dormitorios` (`iddormitorios`, `grandes`, `medianos`, `pequenos`) VALUES ('".$this->idDormitorios."', '".$grandes."', '".$medianos."', '".$pequenos."');");
	}

	// ***********************************************************************************
	public function newFormulario(){

		$this->bd->query("INSERT INTO `formulario` (
			`idtazacion`, 
			`correo`, 
			`idamenidades`, 
			`idbanos`, 
			`idwalkingClosets`, 
			`idbasicos`, 
			`idparqueos`, 
			`idpisos`, 
			`idambientes`, 
			`idclosets`, 
			`iddormitorios`, 
			`idadicionales`
		) VALUES (
			'".$this->idFormulario."', 
			'".$this->idUsuario."', 
			'".$this->idAmenidades ."', 
			'".$this->idBanos ."', 
			'".$this->idWalkingClosets ."', 
			'".$this->idBasics ."', 
			'".$this->idParqueos ."', 
			'".$this->idPisos ."', 
			'".$this->idAmbients ."', 
			'".$this->idClosets ."', 
			'".$this->idDormitorios ."', 
			'".$this->idAdicionales ."'
		);");

		$this->bd->query("UPDATE `tazaciones` SET `solved` = '1' WHERE `tazaciones`.`idtazaciones` = '".$this->idFormulario."';");

		$this->bd->query("INSERT INTO `historial` (`correo`, `idtazaciones`) VALUES ( '".$this->idUsuario."','".$this->idFormulario."');");
	}

	// ***********************************************************************************
	public function getConexion(){
		return $this->bd;
	}

	public function verificacionDuplicados($table,$idTable,$codigo){

	$i = 1;
	$prefijo = $codigo.'_';
	$this->newCodigo = $prefijo.$i;

	do {
		$res = $this->bd->query("SELECT COUNT(*) FROM `".$table."` WHERE ".$idTable." = '".$this->newCodigo."';");
		while($item=$res->fetch(PDO::FETCH_ASSOC)){ $this->formularios[]=$item;}
		foreach ($this->formularios as $formulario) { $rs=$formulario['COUNT(*)'];}
		if ($rs){ $i=$i+1; $this->newCodigo = $prefijo.$i; }
	} while ($rs);

	return $this->newCodigo;
}

}
 ?>