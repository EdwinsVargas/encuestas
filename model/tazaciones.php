<?php

class tazaciones{

	private $bd;
	
	function __construct(){
		require 'conexion.php';

		$this->bd = conexion::conectar();

		$res = $this->bd->query("SELECT users.status FROM `users` WHERE users.correo = '".$_SESSION['correo']."';");

		while($item=$res->fetch(PDO::FETCH_ASSOC)){
		 $aut=$item;}

		if ($aut['status'] == '0'){
			session_destroy(); 
			echo "<meta http-equiv=\"refresh\" content=\"0;URL=../\">";
			}
		
	}

	public function getTazaciones(){

		$res = $this->bd->query("SELECT tazaciones.*, estado.nombre as estado, ciudad.nombre as ciudad, municipio.nombre as municipio FROM tazaciones, estado, ciudad, municipio WHERE tazaciones.idmunicipio = municipio.idmunicipio AND municipio.idciudad = ciudad.idciudad AND ciudad.idestado = estado.idestado order by tazaciones.solved ASC;");
			$tazaciones = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   				$tazaciones[]=$item;
   		}
   		return isset($tazaciones) ? $tazaciones : null;
	}
	public function getTazacionesByCiudad($ciudad){

		$res = $this->bd->query("SELECT t.*, e.nombre as estado, c.nombre as ciudad, m.nombre as municipio FROM tazaciones as t INNER JOIN municipio as m on t.idmunicipio = m.idmunicipio INNER JOIN ciudad as c on m.idciudad = c.idciudad INNER JOIN estado as e on c.idestado = e.idestado WHERE t.solved = '0' and c.idciudad = '".$ciudad."';");
			$tazaciones = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   				$tazaciones[]=$item;
   		}
   		return isset($tazaciones) ? $tazaciones : false;
	}
	public function getTazacionesDisponibles(){

		$res = $this->bd->query("SELECT tazaciones.*, estado.nombre as estado, ciudad.nombre as ciudad, municipio.nombre as municipio FROM tazaciones, estado, ciudad, municipio WHERE tazaciones.idmunicipio = municipio.idmunicipio AND municipio.idciudad = ciudad.idciudad AND ciudad.idestado = estado.idestado AND tazaciones.solved = '0' order by tazaciones.solved ASC;");
			$tazaciones = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   				$tazaciones[]=$item;
   		}

   		return isset($tazaciones) ? $tazaciones : null;
	}

	public function getGaleria($codigo){

		$res = $this->bd->query("SELECT * FROM `galeria` WHERE `idtazacion` LIKE '".$codigo."';");
			$galerias = array();
			while($item=$res->fetch(PDO::FETCH_ASSOC)){
   				$galerias[]=$item;
   			}
   		return $galerias;
	}

	public function getTazacion($codigo){

		$res = $this->bd->query("SELECT tazaciones.*, estado.nombre as estado, ciudad.nombre as ciudad, municipio.nombre as municipio FROM tazaciones, estado, ciudad, municipio WHERE tazaciones.idmunicipio = municipio.idmunicipio AND municipio.idciudad = ciudad.idciudad AND ciudad.idestado = estado.idestado AND tazaciones.idtazaciones = '".$codigo."';");
			$tazaciones = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$tazaciones[]=$item;
   		}
   		return $tazaciones[0];
	}

	public function getReport($codigo){

	$res = $this->bd->query("SELECT * FROM formulario, adicionales, ambientes, amenidades, banos, basicos, closets, dormitorios, parqueos, pisos, walkingclosets WHERE formulario.idadicionales = adicionales.idadicionales AND formulario.idambientes = ambientes.idambientes AND formulario.idamenidades = amenidades.idamenidades AND formulario.idbanos = banos.idbanos AND formulario.idbasicos = basicos.idbasicos AND formulario.idclosets = closets.idclosets AND formulario.iddormitorios = dormitorios.iddormitorios AND formulario.idparqueos = parqueos.idparqueos AND formulario.idpisos = pisos.idpisos AND formulario.idwalkingClosets = walkingclosets.idwalkingClosets AND formulario.idtazacion = '".$codigo."';");
		$report = array();
	while($item=$res->fetch(PDO::FETCH_ASSOC)){
			$report[]=$item;
		}
		return $report[0];
	}

	public function newSolicitud($idmunicipio,$direccion,$cliente,$telefonoCliente,$fechaGeneracion,$codigo){

		$this->bd->query("INSERT INTO `tazaciones` (
			`idtazaciones`, 
			`idmunicipio`, 
			`direccion`, 
			`cliente`, 
			`telefonoCliente`, 
			`fechaGeneracion`, 
			`solved`
			) VALUES (
			'".$codigo."', 
			'".$idmunicipio."', 
			'".$direccion."', 
			'".$cliente."', 
			'".$telefonoCliente."', 
			'".$fechaGeneracion."', 
			'0'
		);");
	}

	public function updateSolicitud($idmunicipio,$direccion,$cliente,$telefonoCliente,$fechaGeneracion,$codigo){

		$this->bd->query("UPDATE `tazaciones` SET `direccion` = '".$direccion."',  `cliente` = '".$cliente."',  `telefonoCliente` = '".$telefonoCliente."' WHERE `tazaciones`.`idtazaciones` = '".$codigo."';");
	}

	public function getEstados(){
		$res = $this->bd->query("SELECT * FROM `estado`");
		$estados = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$estados[]=$item;
   		}
   		return $estados;
	}

	public function getCiudadesDe($idestado){
		$res = $this->bd->query("SELECT * FROM `ciudad` WHERE idestado ='".$idestado."';");
			$ciudades = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$ciudades[]=$item;
   		}
   		return $ciudades;
	}

	public function getCiudades(){
		$res = $this->bd->query("SELECT * FROM `ciudad`");
			$ciudades = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$ciudades[]=$item;
   		}
   		return $ciudades;
	}

	public function getMunicipios(){
		$res = $this->bd->query("SELECT * FROM `municipio`");
			$ciudades = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$ciudades[]=$item;
   		}
   		return $ciudades;
	}

	public function getMunicipiosDe($idciudad){
		$res = $this->bd->query("SELECT * FROM `municipio` WHERE idciudad ='".$idciudad."';");
			$municipio = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$municipios[]=$item;
   		}
   		return $municipios;
	}

	public function verificacionDuplicados($table,$idTable,$codigo){

		$i = 1;
		$prefijo = $codigo.'_';
		$this->newCodigo = $prefijo.$i;

		do {
			$res = $this->bd->query("SELECT COUNT(*) FROM `".$table."` WHERE ".$idTable." = '".$this->newCodigo."';");
			while($item=$res->fetch(PDO::FETCH_ASSOC)){ $this->formularios[]=$item;}
			foreach ($this->formularios as $formulario) { $rs=$formulario['COUNT(*)'];}
			if ($rs){ $i=$i+1; $this->newCodigo = $prefijo.$i; }
		} while ($rs);

		return $this->newCodigo;
	}

	public function getFormulario($codigo){

		$res = $this->bd->query("SELECT * FROM `formulario` WHERE idtazacion = '".$codigo."';");
			$formularios = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$formularios[]=$item;
   		}

   		return count($formularios) > 0 ? $formularios[0] : false;
	}

	public function eliminarTazacion($codigo){

		$formulario = $this->getFormulario($codigo);

	if($formulario){
		$this->bd->query("DELETE FROM `galeria` WHERE `galeria`.`idtazacion` = '".$codigo."';");

		$this->bd->query("DELETE FROM `historial` WHERE `historial`.`idtazaciones` = '".$codigo."';");
		
		$this->bd->query("DELETE FROM `formulario` WHERE `idtazacion` = '".$codigo."';");

		$this->bd->query("DELETE FROM `adicionales` WHERE `adicionales`.`idadicionales` = '".$formulario['idadicionales']."'");

		$this->bd->query("DELETE FROM `ambientes` WHERE `ambientes`.`idambientes` = '".$formulario['idambientes']."'");

		$this->bd->query("DELETE FROM `amenidades` WHERE `amenidades`.`idamenidades` = '".$formulario['idamenidades']."'");

		$this->bd->query("DELETE FROM `banos` WHERE `banos`.`idbanos` = '".$formulario['idbanos']."'");

		$this->bd->query("DELETE FROM `basicos` WHERE `basicos`.`idbasicos` = '".$formulario['idbasicos']."'");

		$this->bd->query("DELETE FROM `closets` WHERE `closets`.`idclosets` = '".$formulario['idclosets']."'");

		$this->bd->query("DELETE FROM `dormitorios` WHERE `dormitorios`.`iddormitorios` = '".$formulario['iddormitorios']."'");

		$this->bd->query("DELETE FROM `parqueos` WHERE `parqueos`.`idparqueos` = '".$formulario['idparqueos']."'");

		$this->bd->query("DELETE FROM `walkingclosets` WHERE `walkingclosets`.`idwalkingclosets` = '".$formulario['idwalkingclosets']."'");

		$this->bd->query("DELETE FROM `tazaciones` WHERE `tazaciones`.`idtazaciones`  = '".$formulario['idtazaciones']."';");

		

		$this->bd->query("DELETE FROM `tazaciones` WHERE `tazaciones`.`idtazaciones`  = '".$codigo."';");

	} else {

		$this->bd->query("DELETE FROM `galeria` WHERE `galeria`.`idtazacion` = '".$codigo."';");

		$this->bd->query("DELETE FROM `historial` WHERE `historial`.`idtazaciones` = '".$codigo."';");

		$this->bd->query("DELETE FROM `tazaciones` WHERE `tazaciones`.`idtazaciones`  = '".$codigo."';");
	}
}
	
	public function getConexion(){

		return $this->bd;
	}

}
 ?>