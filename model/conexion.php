<?php 

    class conexion{

        public static function conectar(){
            require '.env.php';
            try{
                $conexion = new PDO('mysql:host='.$_HOST.';dbname='.$_DATABASE,$_USER,$_PASSWORD);
                $conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $conexion->exec("SET CHARACTER SET UTF8");
            }catch(Exception $e){
                die("Error ".$e->getMessage());
                echo "Linea del error: ". $e->getLine();
            }
            return $conexion;
        }//finaliza CONECTAR
    }
?>