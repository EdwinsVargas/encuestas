<?php

class usuario{

	private $bd;
	private $correo;
	private $clave;
	private $rol;

	function __construct(){
		require_once 'conexion.php';
		$this->bd = conexion::conectar();
	}


	public function setUsuario($correo, $clave){
		$this->correo=$correo;
		$this->clave =$clave;
	}

	public function newSession(){
		$res = $this->bd->query("SELECT * FROM `users` WHERE correo = '".$this->correo."' AND clave = '".$this->clave."';");
		$users = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$users[]=$item;
   		}
   		if (isset($users)){

	   			foreach ($users as $user);

				$_SESSION['status'] = $user['status'];
				
				switch ($_SESSION['status']) {
					case '1':
							$_SESSION['nombre'] = $user['nombre'];
							$_SESSION['correo'] = $user['correo'];
							$_SESSION['clave']  = $user['clave'];
							$_SESSION['rol']    = $user['rol'];
				   		    header('location:../');
						break;

					case '0':
							session_destroy();
							header('location:userBlocked.php');
						break;

					case '':
 							header('location:usuarioInvalido.php');
						break;
					
					default:
							header('location:../');
						break;
				}
			}
	}

		public function getHistorialDe($correo){
			$res = $this->bd->query("SELECT
		    tazaciones.*,
		    historial.correo,
		    estado.nombre AS estado,
		    ciudad.nombre AS ciudad,
		    municipio.nombre AS municipio
		FROM
		    tazaciones,
		    estado,
		    ciudad,
		    historial,
		    municipio
		WHERE
		    tazaciones.idmunicipio = municipio.idmunicipio AND 
		    municipio.idciudad = ciudad.idciudad AND 
		    ciudad.idestado = estado.idestado AND 
		    historial.idtazaciones = tazaciones.idtazaciones AND
	    solved = 1 AND  historial.correo = '".$correo."';");
			$tazaciones = array();
			while($item=$res->fetch(PDO::FETCH_ASSOC)){
	   			$tazaciones[]=$item;
	   		}

   		return isset($tazaciones) ? $tazaciones : null;
	}

	public function isContainGallery($codigo){

		$res = $this->bd->query("SELECT COUNT(*) as count FROM `galeria` WHERE idtazacion = '".$codigo."';");
			$galeria = array();
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$galerias[]=$item;
   		}
   		foreach ($galerias as $galeria);

   		if ($galeria['count'] >= 1) {
   			return true;
   		}else{
   			return false;
   		}
	}

	public function logout(){
		session_destroy();
	}
	
	public function getTazadores(){

		$res = $this->bd->query("SELECT * FROM `users` WHERE rol = 3");

		$tazadores = array();

		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$tazadores[]=$item;
   		}
   		return $tazadores;
	}

	public function getAdministradores(){

		$res = $this->bd->query("SELECT * FROM `users` WHERE rol = 2");

		$administradores = array();

		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$administradores[]=$item;

   		}
   		return $administradores;
	}

	public function eliminarTazador($correo){

		$this->bd->query("DELETE FROM `users` WHERE rol = 3 AND `correo` = '".$correo."';");
		$this->bd->query("DELETE FROM `historial` WHERE `correo` = '".$correo."';");
	}

		public function eliminarAdministrador($correo){

		$this->bd->query("DELETE FROM `users` WHERE rol = 2 AND `correo` = '".$correo."';");
	}

	
	public function getTazador($correo){

		$res = $this->bd->query("SELECT * FROM `users` WHERE rol = 3 AND correo = '".$correo."';");

		$tazadore = array();

		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$tazadores[]=$item;
   		}
   		return $tazadores[0];
	}

	public function getUser($correo){

		$res = $this->bd->query("SELECT * FROM `users` WHERE correo = '".$correo."';");

		$tazadore = array();

		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$tazadores[]=$item;
   		}
   		if ($tazadores[0]['status'] == '0'){
			session_destroy(); 
			echo "<meta http-equiv=\"refresh\" content=\"0;URL=../\">";
			}

   		return $tazadores[0];
	}

	public function getAdministrador($correo){

	$res = $this->bd->query("SELECT * FROM `users` WHERE rol = 2 AND correo = '".$correo."';");

	$tazadores = array();

	while($item=$res->fetch(PDO::FETCH_ASSOC)){
			$tazadores[]=$item;
		}
		return $tazadores[0];
	}


	public function updateUser($nombre,$telefono,$clave,$correo,$status){
	$res = $this->bd->query("UPDATE `users` SET `nombre` = '".$nombre."', `telefono` = '".$telefono."', `clave` = '".$clave."', `status` = '".$status."' WHERE `users`.`correo` = '".$correo."';");
	}


	public function newUser($correo, $nombre,$telefono, $clave, $rol){

		$res = $this->bd->query("SELECT COUNT(*) as count FROM `users` WHERE correo = '".$correo."';");
		
		while($item=$res->fetch(PDO::FETCH_ASSOC)){
   			$users[]=$item;
   		}
   		foreach ($users as $user);

   		if ($user['count'] == 0){

			$res = $this->bd->query("INSERT INTO `users` (`correo`, `nombre`, `telefono`, `clave`, `rol`) VALUES ('".$correo."', '".$nombre."',  '".$telefono."', '".$clave."', '".$rol."');");

		}else{
			switch ($rol) {
				case '1':echo "<script>alert('Root ya esta Registrado');</script>";
					break;
				case '2':echo "<script>alert('Administrador ya esta Registrado');</script>";
					break;
				case '3':echo "<script>alert('Tasador ya esta Registrado');</script>";
					break;
				default:echo "<script>alert('Ups, algo salio mal.');</script>";
				break;
			}
			
		}
	}
}

?>